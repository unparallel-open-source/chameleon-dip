import { NextApiRequest } from "next";
import { IoTCatURL } from "../components/misc";


export interface IframeMethodProps {
	pageName: string,
	filter?: any,
	showAddButton?: boolean,
	addedComponentIds?: string[],
	componentListProps?: any,
	accessToken?: string
}

interface IoTCatParams {
	token: string,
	context: string
}

export function getIoTCatParams(req: NextApiRequest): IoTCatParams {
	const authorization = req.headers.authorization

	if (authorization) return {

		token: process.env.IOT_CAT_TOKEN,
		context: process.env.IOT_CAT_CONTEXT,

	}
	return {
		token: process.env.IOT_CAT_PUBLIC_TOKEN,
		context: process.env.IOT_CAT_PUBLIC_CONTEXT
	}
}

export async function generateIframeURL({ pageName, filter, showAddButton, addedComponentIds, componentListProps, accessToken }: IframeMethodProps): Promise<string> {
	const url = "/api/iframeManager"
	const body = JSON.stringify({
		pageName,
		filter,
		showAddButton,
		addedComponentIds,
		componentListProps,
		origin: location.origin
	})
	const headers: HeadersInit = {
		"Content-Type": "application/json",

	}
	if (accessToken) headers.Authorization = `Bearer ${accessToken}`
	const response = await fetch(url,
		{
			method: "POST",
			headers,
			body
		})
	return response.json()

}

export async function getComponentName(id: string): Promise<string | undefined> {
	if (!id) return
	const response = await fetch(`${IoTCatURL}/api/getTPIElement?access_token=${process.env.IOT_CAT_TOKEN}&pageName=components&id=${id}`)
	const json = await response.json()
	return json?.name
}

export async function getComponent(id: string): Promise<any> {
	if (!id) return
	const response = await fetch(`${IoTCatURL}/api/getTPIElement?access_token=${process.env.IOT_CAT_TOKEN}&pageName=components&id=${id}`)
	return response.json()

}

export async function getComponents(): Promise<any[]> {
	const response = await fetch(`${IoTCatURL}/api/getTPIElementsId?access_token=${process.env.IOT_CAT_TOKEN}&pageName=components`)
	const json = await response.json()
	const ids = json?.map(el => el._id)
	const components = []
	for (const id of ids) {
		const component = await getComponent(id)
		components.push(component)
	}
	return components
}

export function renderNumber(num: number | undefined): number | string {
	if (typeof num === "number") return num
	return "-"
}
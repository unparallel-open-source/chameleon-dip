import FrontendHelper from "@unparallel/frontend-helper";

export async function getKeycloakSession(){
    const fh = new FrontendHelper();
    const session = await fh.getSession(process.env.NEXT_PUBLIC_ROOT_URL);

    return session;
}

export async function getKeycloakToken(){
    const fh = new FrontendHelper();
    const token = await fh.getToken(process.env.NEXT_PUBLIC_ROOT_URL);

    return token;
}

export async function getKeycloakUserID(){
    const fh = new FrontendHelper();
    const userID = await fh.getUserID(process.env.NEXT_PUBLIC_ROOT_URL);

    return userID;
}

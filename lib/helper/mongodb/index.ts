// ****************************** BUNDLES ******************************//

import { Order } from "../../../components/types";



export async function getBundles(filters = {}, projection = undefined) {
    const res = await fetch("/api/bundles/getBundles", {
        method: "POST",
        headers: { "Content-type": "application/json; charset=UTF-8" },
        body: JSON.stringify({
            filters,
            projection
        })
    });
    const data = await res.json();

    return data.bundles;
}

export async function getBundleDefaultOptions(bundleID) {
    const bundle = await getBundles({
        _id: bundleID
    });

    return bundle[0].defaultOptions;
}

// ****************************** IMAGES ******************************//

export async function getImages(filters = {}, projection = undefined) {
    const res = await fetch("/api/images/getImages", {
        method: "POST",
        headers: { "Content-type": "application/json; charset=UTF-8" },
        body: JSON.stringify({
            filters,
            projection
        })
    });
    const data = await res.json();

    return data.images;
}

async function getIoTCatComponent(id: string) {
    const response = await fetch(`/api/component/${id}`)
    return response.json()
}

export function getIoTCatComponents(ids: string[]) {
    return Promise.all(ids.map(getIoTCatComponent))
}

// ****************************** ORDERS ******************************//

export async function addOrder(order) {
    const res = await fetch("/api/orders/addOrder", {
        method: "POST",
        headers: { "Content-type": "application/json; charset=UTF-8" },
        body: JSON.stringify({
            order
        })
    });
    const data = await res.json();

    return data;
}

export async function getOrders(filters: Partial<Order> = {}, projection?: any, options?: any) {
    const res = await fetch("/api/orders/getOrders", {
        method: "POST",
        headers: { "Content-type": "application/json; charset=UTF-8" },
        body: JSON.stringify({
            filters,
            projection,
            options
        })
    });
    const data = await res.json();

    return data.orders;
}

export async function getCurrentOrder() {
    const orders = await getOrders({ status: "PENDING" });
    const currentOrder = orders?.[0]
    if (!currentOrder) {
        await addOrder({
            name: "New Order",
            createdOn: new Date().toISOString(),
            status: "PENDING",
            images: [],
            bundles: [],
            result: {}
        })
        return getCurrentOrder()
    }

    return currentOrder
}

export async function updateOrder(filters, options) {
    const res = await fetch("/api/orders/updateOrder", {
        method: "POST",
        headers: { "Content-type": "application/json; charset=UTF-8" },
        body: JSON.stringify({
            filters,
            options
        })
    });
    const data = await res.json();

    return data;
}

function removeDuplicateObjects(storedArray, newArray) {
    const storedIDs = storedArray.map(elem => elem._id);
    const finalArray = [];

    newArray.forEach(elem => {
        if (!storedIDs.includes(elem._id))
            finalArray.push(elem);
    });

    return finalArray;
}

export async function addBundlesToOrder(bundleIDs) {
    // debugger
    /*const currentBundles = (await getCurrentOrder()).bundles;
    const selectedBundles = await getBundles({ _id: { $in: bundleIDs } }, { defaultOptions: 1 });
    const newBundles = removeDuplicateObjects(currentBundles, selectedBundles);
    console.log("addBundlesToOrder", bundleIDs, newBundles)*/
    const addedBundles = await updateOrder({ status: "PENDING" }, { $addToSet: { bundles: { $each: bundleIDs } } });

    return addedBundles;
}

export async function updateBundleOptions(bundleID, bundleOptions) {
    const order = await getCurrentOrder()


    if (!order) return
    const index = order.bundles?.findIndex(el => el._id === bundleID)
    if (index < 0) return

    const updatedOrder = await updateOrder({ status: "PENDING" }, { $set: { [`bundles.${index}.defaultOptions`]: bundleOptions } });
    return updatedOrder;
}

export async function removeBundleFromOrder(bundleID) {
    const removedBundles = await updateOrder({ status: "PENDING" }, { $pull: { bundles: bundleID } });

    return removedBundles;
}

export async function addImagesToOrder(imageIDs) {


    const currentImages = (await getCurrentOrder()).images;

    const selectedImages = await getImages({ _id: { $in: imageIDs } }, { _id: 1 });
    const newImages = removeDuplicateObjects(currentImages, selectedImages);

    const addedImages = await updateOrder({ status: "PENDING" }, { $addToSet: { images: { $each: imageIDs } } });

    return addedImages;
}

export async function removeImageFromOrder(imageID) {

    const removedImages = await updateOrder({ status: "PENDING" }, { $pull: { images: imageID } });

    return removedImages;
}

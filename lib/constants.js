export const PROJECT_NAME = "CHAMELEON"
export const PROGRAMME_NAME = "Horizon Europe"
export const GRANT_AGREEMENT_NUMBER = "101060529"
export const dataSetTagId = "a265ebe9a8a87e5399be9527"

export const dataModelTagId = "9ce365595b7b7341fcb59c10"

export const bundleTagId = "e8b6b0cb04218ae939265061"

export const executionTagId = "7d026f2162aa4095eb86f37e"
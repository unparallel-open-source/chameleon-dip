

import React, { useEffect, useState, useRef } from 'react'
import DataList, { DataListElement } from "../DataList/DataList";
import { addImagesToOrder, getCurrentOrder, getImages, removeImageFromOrder } from "../../lib/helper"
import Loading from "../Loading/Loading";
import { Dataset, Order } from "../types";
import { getElementsFromOrder } from "../DataList/methods";
import DatasetExplorer from "../DatsetExplorer";
import { useRouter } from 'next/navigation'
import Image from "react-bootstrap/Image";
import FilterIcon from "../icons/filter/index";
import { useIframeResize } from "../useIframeResize";
import classnames from 'classnames';
import { useData } from '../../lib/dataFetch';
import { renderNumber } from '../../lib/methods';

export default function DatasetList() {
	const [elements, setElements] = useState<DataListElement[] | undefined>(undefined)
	const [datasets, setDatasets] = useState<Dataset[] | undefined>(undefined)
	const [order, setOrder] = useState<Order | undefined>(undefined)
	const [disableList, setDisableList] = useState<boolean>(false)
	const router = useRouter()
	const ref = useRef(undefined)
	const { height } = useIframeResize(ref)

	const datasetCount = useData("/api/count/datasets")?.data

	useEffect(() => {
		getImages().then(setDatasets)
		getCurrentOrder().then(setOrder)
	}, [])


	useEffect(() => {

		const elements = getElementsFromOrder(order, datasets, "images")

		if (elements) setElements(elements)

	}, [datasets, order])

	function onElementSelect(elementId: string, checked: boolean) {

		setDisableList(true)
		if (checked) {
			addImagesToOrder([elementId]).then(res => {
				setDisableList(false)
			})
		} else {
			removeImageFromOrder(elementId).then(res => {
				setDisableList(false)
			})
		}
	}
	if (!elements) return <Loading />
	function onComponentAdded(id) {
		addImagesToOrder([id]).then(() => {
			router.push("/")
		})


	}
	function onComponentRemoved(id) {
		removeImageFromOrder(id).then(() => {
			router.push("/")
		})
	}
	if (!order) return <Loading />

	function renderLegend() {
		return (
			<>
				<div className="d-none d-xl-flex" style={{ marginTop: height >= 1058 ? "-79px" : "-30px", position: "relative", width: "fit-content" }}>
					<FilterIcon className={classnames(height < 1000 ? "d-none" : "me-2 align-items-center")} width={20} color="var(--bs-primary)" />
					<p className={classnames(height < 1000 ? "d-none" : "me-4 mb-0")}>CHAMELEON Datasets</p>
					<FilterIcon className={classnames(height < 1000 ? "d-none" : "me-2 align-items-center")} width={20} color="var(--bs-info)" />
					<p className={classnames(height < 1000 ? "d-none" : "me-4 mb-0")}>Other Datasets</p>
				</div>
				<div className="d-none d-lg-flex d-xl-none" style={{ marginTop: height >= 1273 ? "-79px" : "-30px", position: "relative", width: "fit-content" }}>
					<FilterIcon className={classnames(height < 1000 ? "d-none" : "me-2 align-items-center")} width={20} color="var(--bs-primary)" />
					<p className={classnames(height < 1000 ? "d-none" : "me-4 mb-0")}>CHAMELEON Datasets</p>
					<FilterIcon className={classnames(height < 1000 ? "d-none" : "me-2 align-items-center")} width={20} color="var(--bs-info)" />
					<p className={classnames(height < 1000 ? "d-none" : "me-4 mb-0")}>Other Datasets</p>
				</div>
				<div className="d-flex d-lg-none" style={{ marginTop: "-30px", position: "relative", width: "fit-content" }}>
					<FilterIcon className={classnames(height < 1000 ? "d-none" : "me-2 align-items-center mb-4")} width={20} color="var(--bs-primary)" />
					<p className={classnames(height < 1000 ? "d-none" : "me-4")}>CHAMELEON Datasets</p>
					<FilterIcon className={classnames(height < 1000 ? "d-none" : "me-2 align-items-center mb-4")} width={20} color="var(--bs-info)" />
					<p className={classnames(height < 1000 ? "d-none" : "me-4")}>Other Datasets</p>
				</div>
			</>			
		)
	}

	return (
		<>
			<div style={{ position: "relative" }}>
				<Image src="/datasetExplorer.png" width={350} style={{ position: "absolute", zIndex: "-1", marginLeft: "-130px" }} />
			</div>
			<h1 className={"text-dark-green mb-0"} style={{ marginTop: "80px" }}>Dataset Explorer</h1>
			<small className='text-dark-green'>{renderNumber(datasetCount)} Results</small>
			<DatasetExplorer style={{ marginTop: "30px" }} addedComponentIds={order.images} onComponentAdded={onComponentAdded} onComponentRemoved={onComponentRemoved} />
			{renderLegend()}
		</>
	)

}
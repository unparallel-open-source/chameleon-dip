import { renderNumber } from "../../lib/methods";
import styles from "./countingItem.module.css";
import Image from "next/image";

export default function CountingItem({ props }) {
    const { iconPath, iconWidth, iconHeight, iconAlt, number, label } = props;

    return (
        <div className="d-flex flex-column align-items-center flex-sm-row align-items-sm-start">
            <Image src={iconPath} width={iconWidth} height={iconHeight} alt={iconAlt} />
            <div className="d-flex flex-column align-items-center align-items-sm-start ms-2" style={{marginTop: "6px"}}>
                <h3 className="text-primary mb-0">{renderNumber(number)}</h3>
                <h5 className="text-success mb-0">{label}</h5>
                <p className={`small`} style={{ color: "var(--bs-gray-500)" }}>BUNDLES</p>
            </div>
        </div>
    );
}

import { useData } from "../../lib/dataFetch";
import { useSession } from "next-auth/react";

import { Col } from "react-bootstrap";
import Image from "next/image";
import styles from "./welcome.module.css";
import CountingItem from "./countingItem";
import Link from "next/link";
import MenuItem from "./menuItem";

import { Bundle, CartElement, Dataset, ElementCategory, Order } from "../types";
import CartFlow from "../CartFlow/CartFlow";

import { getBundles, getCurrentOrder, getImages, removeImageFromOrder, removeBundleFromOrder, updateOrder } from "../../lib/helper";
import { useEffect, useState } from "react";
import Loading from "../Loading/Loading";

import { updateBundleOptions } from "../../lib/helper";
import { UsbFill } from "react-bootstrap-icons";


const categories = [
    {
        id: "dataset",
        name: "Dataset",
        icon: "dataset"
    },
    {
        id: "bundle",
        name: "Bundle",
        icon: "bundle"
    }
]

async function test() {
    // console.log("Clicked");
    // updateBundleOptions().then(a => console.log(a));
}

export default function Welcome() {
    const { data: session, status } = useSession();

    const [bundles, setBundles] = useState<Bundle[] | undefined>()
    const [datasets, setDatasets] = useState<Dataset[] | undefined>()
    const [order, setOrder] = useState<Order | undefined>()
    const [cartElements, setCartElements] = useState<CartElement[] | undefined>()
    const forestryCount = useData("/api/count/forestry")?.data
    const agricultureCount = useData("/api/count/agriculture")?.data
    const livestockCount = useData("/api/count/livestock")?.data
    useEffect(() => {
        getInfo()

    }, [])


    function getInfo() {
        getCurrentOrder().then(setOrder)
        getImages().then(setDatasets)
        getBundles().then(setBundles)
    }

    useEffect(() => {
        if (!bundles || !datasets || !order) return
        const cartElements: CartElement[] = []

        for (const el of datasets) {
            cartElements.push({ id: el._id, name: el.name, description: el.description, categoryId: "dataset" })
        }
        for (const el of bundles) {

            cartElements.push({ id: el._id, name: el.name, description: el.description, categoryId: "bundle" })
        }
        setCartElements(cartElements)
    }, [bundles, datasets, order])

    async function onCartElementDelete(elId: string, categoryId: string) {

        if (!elId) return
        if (categoryId === "dataset") {
            await removeImageFromOrder(elId)
            const order = await getCurrentOrder()

            setOrder(order)
            getImages().then(setDatasets)
        }
        if (categoryId === "bundle") {
            await removeBundleFromOrder(elId)
            const order = await getCurrentOrder()
            setOrder(order)
            getBundles().then(setBundles)
        }

    }

    function renderCountingItem(iconPath, iconAlt, number, label) {
        return (
            <CountingItem props={{
                iconPath,
                iconWidth: 43,
                iconHeight: 43,
                iconAlt,
                number,
                // number: data[0],
                label
            }} />
        )
    }

    function renderCountingItems() {

        return (
            <div className="d-flex flex-row mt-4 text-center justify-content-between flex-wrap" style={{ maxWidth: "100%" }}>
                {renderCountingItem("/forestryIcon.png", "Forestry icon", forestryCount, "Forestry")}
                {renderCountingItem("/agricultureIcon.png", "Agriculture icon", agricultureCount, "Agriculture")}
                {renderCountingItem("/livestockIcon.png", "Livestock icon", livestockCount, "Livestock")}
                {/*{renderCountingItem("/vineyard.png", "Vineyard icon", 3, "Vineyard")}*/}
            </div>
        )
    }

    function renderMenuItems() {
        return null
        return (
            <>
                {renderMenuItem("/store", "bg-success", "/storeIcon.png", 49, 50, "CHAMELEON Store icon", "Bundles")}
                {renderMenuItem("/orders", "bg-primary", "/ordersIcon.png", 54, 51, "Orders icon", "Orders")}
                {renderMenuItem("/", "bg-secondary", "/droneIcon.png", 87, 42, "Drones icon", "Drones")}
            </>
        )
    }

    function renderMenuItem(link, color, iconPath, width, height, iconAlt, label) {
        return (

            <div className={`mx-5 ${styles.menuItems}`}>
                <Link href={link}>
                    <MenuItem props={{
                        color,
                        iconPath,
                        iconWidth: width,
                        iconHeight: height,
                        iconAlt,
                        label
                    }} />
                </Link>
            </div>
        )
    }


    async function onClose() {
        await updateOrder({ status: "PENDING" }, { $set: { status: "EXECUTING" } })
        getInfo()

    }

    function renderCartFlow() {
        if (!cartElements) return <Loading className={"position-absolute"} />

        return <CartFlow order={order} categories={categories} elements={cartElements} onCartElementDelete={onCartElementDelete} onClose={onClose} />
    }

    function renderBundleBanner() {
        return(
            <>
                <div className="w-100 bg-info rounded-4 d-none d-xl-flex flex-row justify-content-between align-items-center shadow" style={{backgroundImage: "url(/bundlesBannerBackground.png)", backgroundPositionX: "-8%", backgroundSize: "auto 110%", backgroundRepeat: "no-repeat", maxWidth: "587px", paddingLeft: "25px", paddingRight: "25px", paddingTop: "30px", paddingBottom: "30px"}}>
                    <div style={{maxWidth: "55%"}}>
                        <h4 className="text-white"> Discover all the Bundles</h4>
                        <p className="m-0" style={{color: "var(--bs-gray-500)"}}> All the tools developed in CHAMELEON, available in one place</p>
                    </div>
                    <a href="/bundles">
                        <button className={"btn btn-primary text-white rounded-pill px-5"} style={{height: "fit-content"}}>Explore</button>
                    </a>                        
                </div>
                <div className="w-100 bg-info rounded-4 d-flex d-xl-none justify-content-between align-items-center shadow flex-column text-wrap text-center flex-sm-row text-sm-start" style={{ gap: "20px", backgroundImage: "url(/bundlesBannerBackground.png)", backgroundPositionX: "-8%", backgroundSize: "auto 110%", backgroundRepeat: "no-repeat", maxWidth: "454px", paddingLeft: "25px", paddingRight: "25px", paddingTop: "30px", paddingBottom: "30px"}}>
                    <div className="text-wrap">
                        <h4 className="text-white text-wrap"> Discover all the Bundles</h4>
                        <p className="m-0" style={{color: "var(--bs-gray-500)"}}> All the tools developed in CHAMELEON, available in one place</p>
                    </div>
                    <a href="/bundles">
                        <button className={"btn btn-primary text-white rounded-pill px-5"} style={{height: "fit-content"}}>Explore</button>
                    </a>
                </div>
            </>            
        )
    }

    var o = {
        "parameterA": "valueA2",
        "parameterB": "valueB",
        "parameterC": "valueC"
    }
    function onClick() {
        updateBundleOptions("652eb6b686180ee66c26b90c", o).then(console.log)
    }

    return (
        <>
            <div className="container">
                <div className="px-1 px-sm-0 d-flex flex-wrap flex-column align-content-center flex-xl-row justify-content-xl-between align-self-xl-start">
                    <div className="d-flex flex-column justify-content-between" style={{maxHeight: "617px"}}>
                        <Image src="/landingpage background.png" width={433} height={412} style={{ marginLeft: "-113px" }} alt={"Welcome to CHAMELEON"} />
                        <div className="d-flex flex-column align-items-center align-items-xl-start">
                            <div className="w-100" style={{ maxWidth: "443px", marginTop: "-330px", minWidth: 0 }}>
                                <div className="position-relative" style={{ aspectRatio: "542/160" }}>
                                    <Image src={"/welcome.png"} fill alt={"Welcome to CHAMELEON"}/>
                                </div>                                     
                                {renderCountingItems()}                                
                            </div>
                        </div>
                        {renderBundleBanner()}
                    </div>
                    {renderCartFlow()}
                </div>
                
            </div>
            {/*<Image src="/landingpage background.png" width={433} height={412} alt={"Welcome to CHAMELEON"} style={{}}/>*/}
        </>        
    );
}
import styles from "./menuItem.module.css";
import Image from "next/image";

export default function MenuItem({props}){
    const { color, iconPath, iconWidth, iconHeight, iconAlt, label} = props;
    return (
        <div className={`d-flex align-items-center flex-column ${styles.wrapper} `}>
            <div className={`d-flex align-items-center justify-content-center ${color} border-primary ${styles.largeCircle}`}>
                <Image src={iconPath} width={iconWidth} height={iconHeight} alt={iconAlt}/>
            </div>
            <div className={`${styles.labelShift} position-absolute`}>
                <div className={"d-flex flex-column align-items-center justify-content-center"}>
                    <div className={`border-primary ${styles.verticalLine}`}/>
                    <div className={`bg-primary border-primary ${styles.smallCircle}`}/>
                    <div className={`text-primary text-uppercase ${styles.label}`}>
                        {label}
                    </div>
                </div>
            </div>
        </div>
    );
}

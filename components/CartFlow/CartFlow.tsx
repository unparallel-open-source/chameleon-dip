import { CartElement, ElementCategory, Order } from "../types";
import CartDetails from "../CartDetails/CartDetails";
import ExecutionCart from "../ExecutionCart/ExecutionCart";
import styles from './index.module.scss'
import { useEffect, useRef, useState } from "react";
import classnames from "classnames";
import Right from "../icons/Right/Right";
import CustomModal from "../CutomModal/CustomModal";
import BundleList from "../ComponentList/ComponentList";
import DatasetList from "../DatasetList/DatasetList";
import { useRouter } from "next/router";
import { updateOrder } from "../../lib/helper/mongodb/index"
import Loading from "../Loading/Loading";
interface CartFlowProps {
	elements: CartElement[],
	categories: ElementCategory[],
	onCartElementDelete?: (elId: string, categoryId) => void,
	order: Order,
	onClose?: () => void
}


export default function CartFlow({ categories, elements, onCartElementDelete, order, onClose }: CartFlowProps) {
	const [leftAreaState, setLeftAreaState] = useState<"closed" | "opened" | "closing" | "opening">("closed")

	const [showDatasetList, setShowDatasetList] = useState<boolean>(false)
	const [showBundleList, setShowBundleList] = useState<boolean>(false)

	const timeoutIdRef = useRef<any>(null)
	const router = useRouter();




	function renderBundleList() {
		if (!showBundleList) return null
		return (
			<CustomModal isOpen={true} onRequestClose={() => { setShowBundleList(false) }}>
				<BundleList />
			</CustomModal>
		)
	}

	function renderDatasetList() {
		if (!showDatasetList) return null
		return (
			<CustomModal isOpen={true} onRequestClose={() => { setShowDatasetList(false) }}>
				<DatasetList />
			</CustomModal>
		)
	}

	function open() {
		clearTimeout(timeoutIdRef.current)
		setLeftAreaState("opening")
		timeoutIdRef.current = setTimeout(() => {
			setLeftAreaState("opened")
		},
			//@ts-ignore
			Number(styles.animationDuration) * 1000
		)
	}
	function close() {
		clearTimeout(timeoutIdRef.current)
		setLeftAreaState("closing")
		timeoutIdRef.current = setTimeout(() => {

			setLeftAreaState("closed")
		},
			//@ts-ignore
			Number(styles.animationDuration) * 1000
		)
	}

	function getLeftAreaClassName() {
		if (leftAreaState === "closed") return styles.closed
		if (leftAreaState === "opened" || leftAreaState === "opening") return styles.opening
		if (leftAreaState === "closing") return styles.closing

	}
	function haveComponents() {
		if (!elements) return false
		const bundleCount = elements.filter(e => e.categoryId === "bundle").length
		const datasetCount = elements.filter(e => e.categoryId === "dataset").length
		return bundleCount > 0 && datasetCount > 0
	}

	function onClick() {

		if (leftAreaState === "closed") {
			open()
		} else {
			if (!haveComponents()) return
			close()
			onClose?.()
		}
	}


	function renderLeft() {
		return (
			<CartDetails order={order} onClose={close} className={classnames(styles.left, getLeftAreaClassName(), { ["rounded-4"]: leftAreaState !== "opened", ["opacity-0"]: leftAreaState === "closed" })} categories={categories} elements={elements} />
		)

	}

	function renderRightShadow() {
		const applyStyle = leftAreaState !== "opened"
		return <div className={classnames({ [styles["right-shadow"]]: applyStyle, shadow: applyStyle, "rounded-4": applyStyle })} />
	}

	function onCategoryOpen(id: string) {
		if (id === "dataset") router.push("/datasets")
		if (id === "bundle") router.push("/bundles")
	}

	function showSummary() {
		return leftAreaState === "opening" || leftAreaState === "opened"
	}

	return (
		<div className={"d-flex flex-grow-1"} style={{maxWidth: "454px"}}>
			{renderBundleList()}
			{renderDatasetList()}
			{/*<div className={classnames(styles.index, { "rounded-4": leftAreaState === "opened" })}>*/}
				{/*{renderLeft()}*/}
				{/*{renderRightShadow()}*/}
			<div className="mb-0 shadow rounded-4 my-5 d-flex flex-grow-1" style={{maxWidth: "454px", paddingTop: "10px", paddingBottom: "23px", paddingLeft: "30px", paddingRight: "30px"}}>
				<ExecutionCart
					onCategoryOpen={onCategoryOpen}
					showSummary={showSummary()}
					onButtonClick={onClick}
					className={classnames({ ["rounded-4"]: leftAreaState !== "opened" })}
					categories={categories}
					elements={elements}
					onCartElementDelete={onCartElementDelete}
					disableButton={!haveComponents()}
				/>
			</div>

			{/*</div>*/}
		</div>
			



	)

}
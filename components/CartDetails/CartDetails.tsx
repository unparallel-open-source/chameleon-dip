import { CartElement, ElementCategory, Order } from "../types";
import { Title } from "../Title/Title";
import classnames from "classnames";
import Right from "../icons/Right/Right";
import styles from './index.module.scss'
import { icons } from "../icons";
import { ParametersList } from "../ParametersList/ParametersList";
import { updateBundleOptions } from "../../lib/helper";
import { convert } from 'html-to-text'
interface CartDetailsProps {
	elements: CartElement[],
	categories: ElementCategory[],
	className?: string,
	onClose?: () => void,
	order?: Order
}

export default function CartDetails({ elements, categories, className, onClose, order }: CartDetailsProps) {

	function renderCategories() {
		if (!categories) return null
		return categories.map(renderCategory)
	}

	function onChange(key: string, value: string, elId: string) {
		/*const _options = order?.bundles?.find((el => el === elId))?.defaultOptions
		const options = { ..._options, [key]: value }

		updateBundleOptions(elId, options)*/
	}

	function renderElement(element: CartElement, i: number) {

		function renderIcon() {
			const Icon = icons[element.categoryId]
			if (!Icon) return <div className={" mt-5"} />
			return <Icon className={" mt-5"} width={40} />
		}
		let options
		/*if (element.categoryId === "bundle") {
			options = order?.bundles?.find((el => el._id === element.id))?.defaultOptions
		}*/
		const _description = convert(element.description, { wordwrap: false })
		return (
			<div className={"border-bottom border-2 "} key={element.id}>
				{renderIcon()}
				<p className={"h5 text-dark-green mb-1"}>{element.name}</p>
				<p className={styles.description}>{_description}</p>
				<ParametersList options={options} onChange={(key, value) => onChange(key, value, element.id)} />
			</div>
		)
	}

	function renderCategory(category: ElementCategory) {
		if (!elements) return null
		const _elements = elements.filter(el => el.categoryId === category.id)
		if (_elements.length === 0) return null
		return (
			<div key={category.id} className={" w-100"}>
				{_elements.map(renderElement)}
			</div>
		)
	}
	function renderCloseArea() {
		return (
			<div tabIndex={0} role={"button"} className={styles["close-area"]} onKeyDown={e => (e.key === "Enter" || e.key === " ") && onClose?.()} onClick={onClose} >
				<Right width={15} />
			</div>
		)
	}
	return (

		<div className={classnames("bg-light p-3 pe-5 ps-5 d-flex flex-column align-items-start", className)}>

			{renderCloseArea()}

			<Title title={"Cart Details"} className={"mb-3"} />
			{renderCategories()}



		</div>
	)
}
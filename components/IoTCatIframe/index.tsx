import { CSSProperties, useEffect, useRef, useState } from "react";
import { generateIframeURL } from "../../lib/methods";
import Loading from "../Loading/Loading";
import { bundleTagId } from "../../lib/constants";

export interface IoTCatIframeProps {
	pageName: string,
	onComponentAdded?: (id: string) => void,
	onComponentRemoved?: (id: string) => void,
	filter?: any,
	addedComponentIds?: string[],
	style?: CSSProperties,
	componentListProps?: any,
	className?: string
}

import { useSession } from "next-auth/react";
const elementColorsByFilters = [
	{
		color: "#FF0000",
		filter: { tags: { $ne: bundleTagId } }
	}
]
export default function IoTCatIframe({ style, pageName, onComponentAdded, filter, addedComponentIds, onComponentRemoved, componentListProps, className }: IoTCatIframeProps) {
	const _className = className || ""
	const { data: session, status } = useSession();
	const ref = useRef<HTMLIFrameElement>()

	const [src, setSrc] = useState<string | null>(null)
	useEffect(() => {
		if (session === undefined) return
		//@ts-ignore
		const accessToken = session?.accessToken
		generateIframeURL({ pageName, filter, showAddButton: !!session, addedComponentIds, componentListProps, accessToken }).then(setSrc)

	}, [pageName, session]);

	useEffect(() => {
		if (!src || !ref.current) return

		ref.current.contentWindow.postMessage({ action: "setFilter", value: filter }, "*")
	}, [filter])





	function processMessage(event: MessageEvent) {
		if (event.data.height) {
			ref.current.height = event.data.height
		}

		if (event.data.action === "addComponent" && event.data.componentId) {
			onComponentAdded?.(event.data.componentId)

		}
		if (event.data.action === "removeComponent" && event.data.componentId) {
			onComponentRemoved?.(event.data.componentId)

		}

	}

	useEffect(() => {
		window.addEventListener("message", processMessage, false);
		return () => {
			window.removeEventListener("message", processMessage)
		}
	}, [])

	if (!src) return <Loading />
	return (
		<iframe style={style} height={0} ref={ref} className={`w-100 overflow-hidden ${_className}`} src={src} />
	)
}
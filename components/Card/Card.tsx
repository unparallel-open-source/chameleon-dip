import classnames from "classnames";
import styles from './index.module.scss'
import { icons } from "../icons";
import Image from 'next/image'
import { convert } from 'html-to-text'
export interface CardProps {
	icon: string,
	categoryName: string,
	image?: string,
	title: string,
	imageAlt?: string,
	description: string,
	onDelete?: () => void,
	className?: string
}


export default function Card({ description, categoryName, onDelete, image, title, icon, imageAlt, className }: CardProps) {


	function renderDeleteIcon() {
		const Delete = icons.delete
		return (
			<div tabIndex={0} role={"button"} onKeyDown={el => { (el.key === "Enter" || el.key === " ") && onDelete?.() }} onClick={onDelete} className={classnames("d-flex justify-content-center align-items-center  ", styles.delete)}>
				<Delete width={25} />
			</div>
		)
	}

	function renderIcon() {
		if (!icon) return null
		const Icon = icons[icon]
		if (!Icon) return
		return <Icon height={40} />

	}

	function renderHeader() {
		return (
			<div className={"d-flex justify-content-between align-items-end "}>
				{renderIcon()}
				<p className={classnames("flex-grow-1 mb-0 ms-2", styles.text)}>{categoryName}</p>
				{renderDeleteIcon()}
			</div>
		)
	}

	function renderImage() {
		if (!image) return null
		return (
			<Image
				src={image}
				alt={imageAlt || title}
				height={200}
				width={200}
				className={classnames("object-fit-cover ms-3", styles.image)}
			/>
		)
	}

	function renderBody() {
		const _description = convert(description, { wordwrap: false })
		return (
			<div className={"d-flex align-items-end"}>
				<div className={"mb-3"}>
					<p className={"h5 text-dark-green mb-1 mt-5"}>{title}</p>
					<p className={classnames(" mb-0", styles.text)}>{_description}</p>

				</div>
				{renderImage()}
			</div>

		)
	}

	return (
		<div className={classnames("border border-2 border-secondary rounded-4 overflow-hidden position-relative ps-3 pt-3", className)}>
			{renderHeader()}
			{renderBody()}
		</div>
	)
}
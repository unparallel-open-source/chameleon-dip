import styles from './index.module.scss'
import classnames from "classnames";
import lodash from "lodash"
import {useRef} from "react";
export interface ParametersListProps {
	options:{[k:string]:string},
	onChange?:(key:string,value:string)=>void
}

export function ParametersList({options,onChange}:ParametersListProps){

	const timeoutIdRef = useRef<any>()

	function _onChange(key,value){
		clearTimeout(timeoutIdRef.current)
		timeoutIdRef.current = setTimeout(()=>{
			onChange?.(key,value)
		},500)

	}



	function renderOption(key:string){
		const value = options[key]
		return (
			<div key={key} className={"d-flex justify-content-between align-items-center pb-3"}>
				<p className={"mb-0 text-secondary"}>{key}</p>

				<input onChange={e=>{

					_onChange(key,e.currentTarget.value)
				}} defaultValue={value} type="text" id="inputPassword5" className={"form-control rounded rounded-pill w-auto"}  aria-describedby="option value"/>
			</div>
		)
	}

	function renderBody(){
		const keys = Object.keys(options)
		return (
			<div className={classnames(styles.body,"bg-white ps-3 pe-3")} >
				{keys.map(renderOption)}
			</div>
		)
	}

	if(!options) return null
	return (
		<div className={"w-100 "}>
			<div className={classnames(styles.header,"align-items-center justify-content-center d-flex text-primary  rounded rounded-pill")} tabIndex={0} role={"button"}>Configure Parameters</div>
			{renderBody()}
		</div>
	)
}
import React, {useEffect, useState} from "react";
export function useIframeResize(ref:any){
	const [height, setHeight] = useState<number >(0)
	useEffect(()=>{
		function listener(event:MessageEvent){
			if(event?.data?.action === "resize"){
				if(ref.current) {
					ref.current.height = event.data.height
				}
				
				setHeight(event.data.height)
			}
		}
		window.addEventListener(
			"message",
			listener,
			false,
		);

		return ()=>{
			window.removeEventListener(
				"message",
				listener,
				false,
			);
		}
	},[])
	return {height}
}

import { useEffect, useRef, useState } from "react";
import { IoTCatURL } from "../misc.js"

import IoTCatIframe from "../IoTCatIframe";


export default function DatasetExplorer({ onComponentAdded, addedComponentIds, onComponentRemoved, style }: any) {
	return <IoTCatIframe style={style} pageName={"dataExplorerNoBar"} addedComponentIds={addedComponentIds} onComponentAdded={onComponentAdded} onComponentRemoved={onComponentRemoved} />
} 
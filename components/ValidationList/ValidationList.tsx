

import React, { CSSProperties, useEffect, useState } from 'react'
import DataList, { DataListElement } from "../DataList/DataList";
import { getBundles, addBundlesToOrder, getCurrentOrder, removeBundleFromOrder } from "../../lib/helper/index"
import Loading from "../Loading/Loading";
import { Bundle, Order } from "../types";
import { getElementsFromOrder } from "../DataList/methods";
import IoTCatIframe from "../IoTCatIframe";
import { fi } from 'date-fns/locale';

export interface ValidationListProps {

    filter?: any,

    style?: CSSProperties,
    //   componentListProps?: any
}

export default function ValidationList({ style, filter }: ValidationListProps) {




    return (
        <>
            <IoTCatIframe style={style} pageName={"validationListNoBar"} filter={filter} />
        </>

    )
}
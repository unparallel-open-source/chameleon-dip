import classnames from "classnames";

export default function Loading({className}:{className?:string}){
	return (
		<h1 className={classnames("w-100 text-center text-primary mt-5",className)}>Loading...</h1>
	)
}
import styles from "./login.module.css";
import Image from "react-bootstrap/Image";
import {Button} from "react-bootstrap";
import {signIn} from "next-auth/react";

export default function Login() {
    return (
        <div className={"d-flex w-100 align-items-center justify-content-center position-relative"}>
            <div>
                <Image className={styles.bgImage} src={"/loginCircleBackground.png"} width={365} height={365} alt={"Rounded chameleon tail"} roundedCircle/>
            </div>

            <div className={"position-absolute"}>
                <div>
                    <Image src={"/logoWithText.png"} width={258} height={76} alt={"CHAMELEON logo with text"}/>
                </div>
                <div className={"d-flex align-items-center justify-content-center mt-4"}>
                    <Button className={`${styles.button} text-success`} onClick={() => signIn("keycloak")}>
                        <div className={"d-flex align-items-center px-2"}>
                            <Image className={"me-3"} src={"/keycloakLogo.png"} width={25} height={25} alt={"Keycloak logo"}/>
                            Sign in with Keycloak
                        </div>
                    </Button>
                </div>
            </div>
        </div>
    );
}

import classnames from 'classnames'
import { DragEventHandler, useEffect, useState } from "react";

import styles from "./index.module.scss"
import { Title } from "../Title/Title";
import { CartElement, ElementCategory } from "../types";
import Card from "../Card/Card";
import { icons } from "../icons";

import { useSession } from "next-auth/react";
import Image from "next/image";

interface ExecutionCartProps {
	className?: string,
	categories: ElementCategory[],
	onCategoryOpen?: (categoryId: string) => void,
	onButtonClick?: () => void,
	showSummary?: boolean,
	elements: CartElement[],
	onCartElementDelete?: (elId: string, categoryId) => void,
	disableButton?: boolean
}


interface UploaderFile {
	file: File,
	uploaderId: string
}

export default function ExecutionCart({ className, categories, onCategoryOpen, onButtonClick, showSummary, elements, onCartElementDelete, disableButton }: ExecutionCartProps) {

	const { data: session, status } = useSession();

	function renderTitle() {
		//return <Title title={"Cart Summary"} className={"mb-3"} />
		return <p style={{color: "var(--bs-gray-500)", marginBottom: "10px"}}>Cart Summary</p>

	}

	function renderButton() {
		let title
		if(session){
			title = showSummary ? "Execute" : "Start Execution"
		}
		else {
			title = "Coming Soon"
		}
		
		return (
			<div className={"flex-grow-1 d-flex align-items-end"}>
				<button onClick={onButtonClick} disabled={disableButton} className={"btn btn-primary text-white rounded-pill ps-5 pe-5"}><span className={"small"}>{title}</span></button>
			</div>
		)
	}

	const handleDragEvent: DragEventHandler<HTMLDivElement> = (event) => {
		return
		event.stopPropagation()

		event.preventDefault()
		event.dataTransfer.dropEffect = "copy"
	}

	const handleDropEvent = (event: any, uploaderId: string) => {
		return
		event.preventDefault()
		event.stopPropagation()
		const _files = [...event.dataTransfer.files]
		const uploaderFiles: UploaderFile[] = _files.map(file => ({ file, uploaderId }))
		//_setFiles([...files,...uploaderFiles])

	}

	function importData(categoryId: string) {
		onCategoryOpen?.(categoryId)
		/*let input = document.createElement('input');
		input.type = 'file';
		input.onchange = _ => {
			// you can use this method to get file and perform respective operations
			const _files =   Array.from(input.files);
			const uploaderFiles:UploaderFile[] = _files.map(file=>({file,uploaderId}))
			_setFiles([...files,...uploaderFiles])
		};
		input.click();*/

	}

	function renderElement(element: CartElement) {
		const icon = element.categoryId
		let category
		if (element.categoryId === "bundle") category = "Bundle"
		if (element.categoryId === "dataset") category = "Dataset"

		return (
			<Card
				key={element.id}
				icon={icon}
				description={element.description}
				categoryName={category}
				title={element.name}
				className={classnames("flex-shrink-0", styles.card)}
				onDelete={() => onCartElementDelete?.(element.id, element.categoryId)}

			/>
		)
	}

	//onDragOver
	function renderCategory(category: ElementCategory) {
		if (!category) return null
		const { icon, name, id } = category

		const _elements = elements?.filter(el => el.categoryId === category.id) || []

		if (_elements.length > 0) return _elements.map(renderElement)
		const Icon = icons[category.icon]
		function renderIcon() {
			if (!Icon) return null
			return <Icon width={40} />
		}
		return (
			<div
				tabIndex={0}
				role={"button"}
				onKeyDown={e => { (e.key === "Enter" || e.key === " ") && importData(category.id) }}
				onClick={() => { importData(category.id) }}
				onDragOver={handleDragEvent}
				onDrop={(ev) => handleDropEvent(ev, id)}
				key={id}
				className={classnames("w-100 border-0 rounded-4 mb-3 d-flex flex-column align-items-center pt-5 pb-5 ps-3 pe-3", styles.category)}
				style={{height:"201px"}}>
				{renderIcon()}
				<p className={"h5 fw-light text-dark-green mb-1"}>Choose a {name}</p>
				<p className={"small fw-bolder mb-0"} style={{color: "var(--bs-gray-500"}}>Select a <span className={"text-lowercase"}>{name}</span> to proceed with the order</p>

			</div>
		)

	}

	function renderRegisterTitle() {
		return (
			<>
				<h1 className='text-dark-green mb-0 d-none d-sm-flex' style={{marginTop: "-35px"}}>Register<br/> to Use Bundles</h1>
				<h1 className='text-dark-green mb-0 text-center d-flex d-sm-none text-sm-start' style={{marginTop: "20px"}}>Register<br/> to Use Bundles</h1>
			</>
		)
	}


	function renderBoxes() {
		if(session){
			return (
				<div className={"w-100"} style={{marginBottom: "7px"}}>
					{categories?.map(renderCategory)}
				</div>
			)			
		}
		else{
			return (
				<div className={classnames('w-100 rounded-4 d-flex flex-column align-items-center align-items-sm-start', styles.category)} style={{padding: "87px 30px", marginBottom: "23px"}}>
					<div className='d-flex w-100 flex-row justify-content-center justify-content-sm-end' style={{gap: "30px"}}>
						<Image src="/datasetIcon.png" width={60} height={51} alt={"Datasets"}/>
						<Image src="/bundleIcon.png" width={46} height={51} alt={"Bundles"}/>						
					</div>
					{renderRegisterTitle()}
					<h5 className='text-primary mb-0 text-center text-sm-start' style={{marginTop: "25px"}}>Explore the bundle execution functionality</h5>
					<h6 className='mb-0 text-center text-sm-start' style={{marginTop: "21px", color: "var(--bs-gray-500"}}>Select a bundle, the input data and the execution parameters by adding them to the cart and place an execution order</h6>
				</div>
			)
		}
	}


	return (
		<div className={classnames("bg-white p-0  d-flex flex-column flex-grow-1 align-items-center", className)}>
			{renderTitle()}
			{renderBoxes()}
			{renderButton()}
		</div>
	)
}
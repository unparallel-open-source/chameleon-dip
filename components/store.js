import ExecutionCart from "./ExecutionCart/ExecutionCart";
import {useCart} from "cart";


const uploaders = [
    {id:"dataset",name:"Dataset",icon:"xx"},
    {id:"bundle",name:"Bundle",icon:"xx"}
]

export default function Store() {
    const {
        addToCart,
        cartItems,
        clearCart,
        decreaseItem,
        toggleCart,
        isCartOpen,
    } = useCart()
    return (
        <div>
            {JSON.stringify(cartItems)}
            <ExecutionCart uploaders={uploaders} className={"rounded-4 shadow"}/>
        </div>
    );
}

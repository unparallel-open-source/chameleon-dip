import { useSession } from "next-auth/react";

import Head from "next/head";

import Header from "./header";
import globals from "../../styles/globals.module.css";
import Login from "../login/login";
import Footer from "./footer";
import { PROJECT_NAME, PROGRAMME_NAME, GRANT_AGREEMENT_NUMBER } from "../../lib/constants";
import Loading from "../Loading/Loading"
export const siteTitle = "CHAMELEON Drone Innovation Platform";

export default function Layout({ children, home }) {
    const { data: session } = useSession();

    function renderChildren() {
        //if (session === null) return <Login />
        /*if (session)*/ return <div className= "mb-5 mb-xxl-5" style={{ minHeight: "800px" }}>{children}</div>
        //return <Loading />
    }
    //welcome.png

    return (
        <div className={`d-flex flex-column vw-100 vh-100 ${globals.font}`} style={{overflowX: "hidden"}}>

            {/*<link rel={"icon"} href={"/favicon.ico"} />*/}
            {/*<meta
                name={"description"}
                content={"Learn how to build a personal website using Next.js"}
            />*/}
            <meta
                property={"og:image"}
                content={"https://chameleon-dip.demos.unparallel.pt/welcome.png"}
            />
            <meta name={"og:title"} content={siteTitle} />
            <meta name={"twitter:card"} content={"summary_large_image"} />

            <header>
                {/*<header style={{'backgroundColor': 'rbga(25, 97, 280, 0)'}}>*/}
                <Header/>
            </header>

            <main  style={{ overflow: "hidden", flexShrink:0 }} >
                {renderChildren()}
            </main>

            <footer style={{backgroundColor: "#D6D6C0", boxShadow: "0 100vh 0 100vh #D6D6C0"}}>
                <Footer projectName={PROJECT_NAME} programmeName={PROGRAMME_NAME} grantAgreementNumber={GRANT_AGREEMENT_NUMBER} />
            </footer>
            
        </div>
    );
}
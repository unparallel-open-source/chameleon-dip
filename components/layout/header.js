import { useSession, signOut, signIn } from "next-auth/react";

import { NavDropdown, Nav, Navbar } from "react-bootstrap";
import styles from "./header.module.css";
import Image from "next/image";
import Link from "next/link";

export default function Header() {
    const { data: session } = useSession();

    function renderDropdown() {
        if (session) {
            return (
                <div>
                    <NavDropdown.Item disabled>
                        {session.user.name}
                    </NavDropdown.Item>
                    <NavDropdown.Divider />
                    <NavDropdown.Item as={Link} href={"/executions"} className={"me-3 w-auto"}>
                        My Executions
                    </NavDropdown.Item>
                    <NavDropdown.Item>
                        <div onClick={() => signOut()}>
                            Sign out
                        </div>
                    </NavDropdown.Item>
                </div>
            )
        }
        else
            return (
                <NavDropdown.Item>
                    <div onClick={() => signIn("keycloak")}>
                        Sign in
                    </div>
                </NavDropdown.Item>
            )

    }


    return (
        <div className={styles["navbar-border"]}>

            <Navbar className={`container px-3 px-sm-0`}>

                <Link href={"/"}>

                    <Navbar.Brand >
                        <Image src={"/monoLogo.png"} width={25} height={25} alt={"CHAMELEON monochromatic logo"} />
                    </Navbar.Brand>
                </Link>


                {/*<Navbar.Toggle aria-controls="basic-navbar-nav"/>*/}

                {/*<Navbar.Collapse id="basic-navbar-nav">*/}
                <Nav className={"justify-content-end align-items-center w-100"}>

                    <NavDropdown.Item as={Link} href={"/bundles"} className={"me-3 w-auto"}>
                        Bundles
                    </NavDropdown.Item>
                    <NavDropdown.Item as={Link} href={"/datasets"} className={"me-3 w-auto"}>
                        Datasets
                    </NavDropdown.Item>
                    <NavDropdown.Item as={Link} href={"/executions"} className={"me-3 w-auto"}>
                        Executions
                    </NavDropdown.Item>



                    <NavDropdown className={styles["dropdown-container"]} title={
                        "More"
                    } align={"end"}>

                        <NavDropdown.Item as={Link} href={"/datamodels"} >
                            Data models
                        </NavDropdown.Item>

                    </NavDropdown>
                    {
                        /* <Nav.Item className={"d-flex align-items-center px-2"}>
                              <Image src={"/searchIcon.png"} width={24} height={24} alt={"Search icon"} />
                          </Nav.Item>*/
                    }

                    <NavDropdown className={styles["dropdown-container"]} title={
                        <Image src={"/userIcon.png"} width={24} height={24} alt={"User icon"} />
                    } align={"end"}>
                        {renderDropdown()}


                    </NavDropdown>
                </Nav>
                {/*</Navbar.Collapse>*/}


            </Navbar>
        </div>

    );
}

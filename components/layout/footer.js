import styles from "./footer.module.css";
import Image from "react-bootstrap/Image";
import Link from "next/link";

export default function Footer({projectName, programmeName, grantAgreementNumber}){

    return (
        
        <div className="py-2 ">
            <div className="container d-flex flex-row align-items-center justify-content-between px-3 px-sm-0">
                <Image src={"/FundedByEU.png"} width={135} height={30} alt={"EU flag"}/>
                <div className="d-flex flex-row" style={{gap: "15px"}}>
                    <Link href="https://x.com/chameleon_heu">
                        <Image src={"/X logo.png"} width={15} height={15} alt={"X logo"}/>
                    </Link>
                    <Link href="https://www.linkedin.com/in/chameleonheu/">
                        <Image src={"/linkedin logo.png"} width={15} height={15} alt={"X logo"}/>
                    </Link>
                    <Link href="https://www.youtube.com/@chameleonheu2023">
                        <Image src={"/youtube logo.png"} width={19} height={15} alt={"X logo"}/>
                    </Link>                    
                </div>                
            </div>
        </div>
        
    );
}

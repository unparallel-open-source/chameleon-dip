

import React, { CSSProperties, useEffect, useState } from 'react'
import DataList, { DataListElement } from "../DataList/DataList";
import { getBundles, addBundlesToOrder, getCurrentOrder, removeBundleFromOrder } from "../../lib/helper/index"
import Loading from "../Loading/Loading";
import { Bundle, Order } from "../types";
import { getElementsFromOrder } from "../DataList/methods";
import IoTCatIframe from "../IoTCatIframe";
import { fi } from 'date-fns/locale';

export interface ComponentListProps {

	filter?: any,
	showAddButton?: boolean,
	onComponentAdded?: (id: string) => void,
	onComponentRemoved?: (id: string) => void,
	addedComponentIds?: string[],
	style?: CSSProperties,
	componentListProps?: any,
	className?: string
}

export default function ComponentList({ style, filter, showAddButton, className, onComponentAdded, onComponentRemoved, addedComponentIds, componentListProps }: ComponentListProps) {

	const [elements, setElements] = useState<DataListElement[] | undefined>(undefined)
	const [bundles, setBundles] = useState<Bundle[] | undefined>(undefined)
	const [order, setOrder] = useState<Order | undefined>(undefined)
	const [disableList, setDisableList] = useState<boolean>(false)

	async function _setCurrentOrderState() {
		const order = await getCurrentOrder()
		setOrder(order)
	}

	useEffect(() => {
		getBundles().then(setBundles)
		_setCurrentOrderState()
	}, [])

	useEffect(() => {
		const elements = getElementsFromOrder(order, bundles, "bundles")
		if (elements) setElements(elements)

	}, [bundles, order])

	if (!elements) return <Loading />

	async function removeCurrentBundle() {
		/*const id = order?.bundles?.[0]?._id
		if (!id) return
		await removeBundleFromOrder(id)*/
	}

	function onElementSelect(elementId: string, checked: boolean) {


		if (checked) {
			setDisableList(true)
			removeCurrentBundle().then(() => {
				addBundlesToOrder([elementId]).then(res => {

					_setCurrentOrderState().then(() => {
						setDisableList(false)
					})

				})
			})

		}
	}

	return (
		<>
			<IoTCatIframe style={style} className={className} componentListProps={componentListProps} pageName={"componentListNoBar"} filter={filter} onComponentAdded={onComponentAdded} onComponentRemoved={onComponentRemoved} addedComponentIds={addedComponentIds} />
		</>

	)
}
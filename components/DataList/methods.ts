import {DataListElement} from "./DataList";
import {Order} from "../types";


export function incrementElement(elementId,elements:DataListElement[] ):DataListElement[]{
	if(!elementId || !elements) return elements
	const index = elements.findIndex(el=>el._id === elementId)
	if(index<0) return elements
	const _elements = [...elements]
	const _element = _elements[index]
	const quantity = ( _element.quantity || 0) + 1
	_elements[index] = {..._elements[index],quantity}
	return _elements
}
export function decrementElement(elementId,elements:DataListElement[] ):DataListElement[]{
	if(!elementId || !elements) return elements
	const index = elements.findIndex(el=>el._id === elementId)
	if(index<0) return elements
	const _elements = [...elements]
	const _element = _elements[index]
	const quantity = ( _element.quantity || 0) - 1
	if(quantity<0) return elements
	_elements[index] = {..._elements[index],quantity}
	return _elements
}


interface ElementWithId {
	_id:string,
	[k:string]:any
}

export function getElementsFromOrder(order:Order | undefined,array:ElementWithId[] | undefined, fieldName:string | undefined):DataListElement[] | undefined {

	if(!order || !array || !fieldName) return
	const selectedBundles = order[fieldName]?.map(el=>el._id) || []
	return array.map(el=>(
		{
			_id:el._id,
			name:el.name,
			_selected:selectedBundles.indexOf(el._id)>=0
		}
	))
}

import styles from './index.module.scss'
import classnames from "classnames";
import {LegacyRef, Ref, useEffect, useRef} from "react";
export interface DataListElement {
	name:string,
	_id:string,
	image?:string,
	_selected?:boolean,
	[k:string]:any
}

export interface DataListProps {
	elements:DataListElement[],
	onSelectElement:(elementId:string,checked:boolean)=>void,
	singleSelection?:boolean,
	disabled?:boolean
}


export default function DataList({elements,onSelectElement,singleSelection,disabled}:DataListProps){


	const ref = useRef<HTMLDivElement>()

	useEffect(()=>{
		const inputs = Array.from(ref.current?.querySelectorAll("input") || [])

		let i = 0
		for(const input of inputs){

			input.checked = elements[i++]._selected

		}
	},[elements])

	function handleInput(ref:HTMLInputElement, element:DataListElement){
		return
		if(!ref) return
		ref.checked = element._selected
	}

	function _onSelectElement(elementId:string,checked:boolean){

			onSelectElement?.(elementId,checked)

	}


	function renderCheckbox(element:DataListElement){
		if(singleSelection){
			return(
			<div className="form-check">
				<input disabled={disabled} ref={(ref)=>handleInput(ref,element)} onClick={(ref)=>{_onSelectElement(element._id,ref.currentTarget.checked)}}   className={classnames(styles.checkbox,"form-check-input")} type="radio" name="flexRadioDefault" id="flexRadioDefault1"/>

			</div>
			)
		}

		return (
			<div className="form-check">
				<input disabled={disabled} ref={(ref)=>handleInput(ref,element)} onClick={(ref)=>{_onSelectElement(element._id,ref.currentTarget.checked)}}  className={classnames(styles.checkbox,"form-check-input")} type="checkbox" value="" id="flexCheckDefault">
				</input>
			</div>
		)
	}

	function renderElement(element:DataListElement){
		return (
			<div key={element._id} className={"d-flex align-items-center border-bottom border-white pb-2 mb-2"}>
				<div className={"flex-grow-1"}>{element.name}</div>
				<div className={"flex-shrink-0 ms-3"}>
					{renderCheckbox(element)}
				</div>
			</div>
		)
	}
	function renderHeader(){
		return null
	}

	return (
		<div ref={ref}>
			{renderHeader()}
			{elements?.map(renderElement)}
		</div>
	)

}
import React from 'react'
import Modal from 'react-modal';
import styles from "./index.module.scss"
const customStyles = {
	content: {
		top: '50%',
		left: '50%',
		right: 'auto',
		bottom: 'auto',
		marginRight: '-50%',
		transform: 'translate(-50%, -50%)',
		zIndex:4000,
		borderRadius:"var(--bs-border-radius-xl)"
	},
};

interface CustomModalProps {
	title?:string,
	isOpen?:boolean,
	afterOpenModal?:()=>void,
	onRequestClose?:()=>void,
	children?:React.ReactElement
}

export default function CustomModal({isOpen, afterOpenModal,onRequestClose,children,title}:CustomModalProps){


	function getBodyElement(){
		if(typeof document !== "undefined"){
			return document.querySelector("body")
		}
	}

	return (


			<Modal
				appElement={getBodyElement()}
				isOpen={isOpen}
				onAfterOpen={afterOpenModal}
				onRequestClose={onRequestClose}
				overlayClassName={styles.overlay}
				style={customStyles}
				contentLabel={title}
			>
				{children}
			</Modal>

	);
}
import classnames from "classnames";

export default interface TitleProps {
	title:string,
	className?:string
}


export function Title({title,className}:TitleProps){
	function renderTitle(){
		return (
			<p className={"h4 text-center text-dark-green mb-3"}>{title}</p>
		)
	}
	function renderLine(){
		return <div className={"line line-primary"} />
	}
	return (
		<div className={classnames("d-flex flex-column align-items-center",className)}>
			{renderTitle()}
			{renderLine()}
		</div>
	)
}
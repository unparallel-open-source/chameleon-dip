import styles from "./index.module.scss"
import {IconProps} from "../index";


export default function Right({width,height,className}:IconProps){
	return (
		<svg className={className} xmlns="http://www.w3.org/2000/svg" style={{width,height}} viewBox="0 0 8.72 15.45">

			<g id="e89486de-d43a-47f0-89de-9ae4c1413d39" data-name="Layer 1">
				<g>
					<line className={styles.main} x1="1" y1="1" x2="7.72"
						  y2="7.72"/>
					<line className={styles.main} x1="1" y1="14.45" x2="7.72"
						  y2="7.72"/>
				</g>
			</g>
		</svg>

	)
}
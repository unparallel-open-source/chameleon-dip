import {Bundle} from "./Bundle/Bundle";
import Dataset from "./Dataset/Dataset";
import Right from "./Right/Right";
import Delete from "./Delete/Delete";

export interface IconProps {
	width?:number,
	height?:number,
	className?:string
}

export const icons:{[key:string]:({width,height,className}: IconProps) => React.ReactElement} = {
	"bundle":Bundle,
	"dataset":Dataset,
	"right":Right,
	"delete":Delete

}
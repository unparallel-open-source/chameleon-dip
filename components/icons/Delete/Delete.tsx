

import styles from "./index.module.scss"
import {IconProps} from "../index";


export default function Delete({width,height,className}:IconProps){
	return (
		<svg className={className} style={{width,height}} id="uuid-d53e472d-3cdf-4e34-9ccc-45fff5c165c1" data-name="Layer 1" xmlns="http://www.w3.org/2000/svg"
			 viewBox="0 0 17 17">

			<path className={styles.main}
				  d="m6,6.5c.28,0,.5.22.5.5v6c0,.28-.22.5-.5.5s-.5-.22-.5-.5v-6c0-.28.22-.5.5-.5Zm2.5,0c.28,0,.5.22.5.5v6c0,.28-.22.5-.5.5s-.5-.22-.5-.5v-6c0-.28.22-.5.5-.5Zm3,.5c0-.28-.22-.5-.5-.5s-.5.22-.5.5v6c0,.28.22.5.5.5s.5-.22.5-.5v-6Z"/>
			<path className={styles.main}
				  d="m15,4c0,.55-.45,1-1,1h-.5v9c0,1.1-.9,2-2,2h-6c-1.1,0-2-.9-2-2V5h-.5c-.55,0-1-.45-1-1v-1c0-.55.45-1,1-1h3.5c0-.55.45-1,1-1h2c.55,0,1,.45,1,1h3.5c.55,0,1,.45,1,1v1Zm-10.38,1l-.12.06v8.94c0,.55.45,1,1,1h6c.55,0,1-.45,1-1V5.06l-.12-.06h-7.76Zm-1.62-1h11v-1H3v1Z"/>
		</svg>

	)
}
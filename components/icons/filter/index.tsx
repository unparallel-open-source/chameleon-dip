import { IconProps } from "../index";

export interface FilterIcon extends IconProps {
    color?: string
}

export default function FilterIcon({ className, height, width, color }: FilterIcon) {
    const _color = color || "var(--bs-primary)"
    return (
        <svg id="Layer_1" style={{ width, height }} className={className} data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 15.02 15.02">
            <defs>

            </defs>
            <path style={{ strokeWidth: 0, fill: _color }} d="M7.51,0c-1.44,0-2.79.41-3.93,1.12.42-.15.87-.25,1.35-.25,2.24,0,4.05,1.81,4.05,4.05s-1.81,4.05-4.05,4.05S.87,7.16.87,4.92c0-.47.1-.92.25-1.35-.71,1.14-1.12,2.49-1.12,3.93,0,4.15,3.36,7.51,7.51,7.51s7.51-3.36,7.51-7.51S11.66,0,7.51,0Z" />
        </svg>
    )
}

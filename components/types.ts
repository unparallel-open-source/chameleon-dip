//Image is already a native type
export interface ImageElement {
	_id: string,
	type: string[],
	location: string,
	domain: string,
	takeOn: Date,
	name: string,
	description: string
}

export interface Bundle {
	_id: string,
	name: string,
	description: string,
	domain: string,
	owner: string,
	uploadedOn: Date
}


export interface OrderBundle {
	"_id": string
	"defaultOptions": { [key: string]: string }
}
export interface OrderImage {
	"_id": string
	"defaultOptions": { [key: string]: string }
}



export interface Order {
	"_id": string,
	"name": string,
	"userID": string,
	"createdOn": string | Date,
	"images": string[],// OrderImage[],
	"bundles": string[],// OrderBundle[],
	"status": "PENDING" | "TO_BE_EXECUTED" | "EXECUTING" | "EXECUTED",
	"executedOn": string | Date,
	"result": {}
}
export interface Dataset {
	"_id": string,
	"name": string,
	description: string
}
export interface ElementCategory {
	id: string,
	icon: string,
	name: string
}

export interface CartElement {
	id: string,
	name: string,
	description: string,
	categoryId: string,
	[key: string]: any
}

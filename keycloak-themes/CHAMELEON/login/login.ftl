<#import "template.ftl" as layout>
<@layout.registrationLayout displayMessage=!messagesPerField.existsError('username','password') displayInfo=realm.password && realm.registrationAllowed && !registrationDisabled??; section>
    <#if section = "form">
        <div class="card border-0 overflow-hidden" style="background-image: url(${url.resourcesPath}/img/loginBackground.png); background-position: 36% 63%; background-size: 154%; background-repeat: no-repeat; margin-top: 100px; background-color: #e7e8c5 !important; border-radius: 100px">
            <div style="padding-left: 80px; padding-right: 80px; padding-top: 41px; padding-bottom: 66px">

                <#--  Image  -->
                <img src="${url.resourcesPath}/img/logoWithText.png" class="rounded-3" style="max-height: 94px; max-width: 100%"/>                  

                <#--  Form  -->
                <form class="d-flex flex-column" onsubmit="login.disabled = true; return true;" action="${url.loginAction}" method="post">    

                    <!-- Username -->
                    <div style="margin-top: 37px">
                        <#if usernameEditDisabled??>
                            <input tabindex="1" type="text" class="form-control rounded-pill" id="username" name="username" value="${(login.username!'')}" disabled>
                        <#else>
                            <input tabindex="1" type="text" class="form-control rounded-pill" id="username" name="username" value="${(login.username!'')}" autofocus placeholder="Username or Email">
                        </#if>
                        <#-- <label for="username"><#if !realm.loginWithEmailAllowed>${msg("username")}<#elseif !realm.registrationEmailAsUsername>${msg("usernameOrEmail")}<#else>${msg("email")}</#if></label> -->
                    </div>

                    <!-- Pw -->
                    <div class=" mt-3">
                        <input tabindex="2" type="password" class="form-control rounded-pill" id="password" name="password" value="${(login.username!'')}" autofocus placeholder="Password">
                        <#-- <label for="password">${msg("password")}</label> -->
                    </div>

                    <#--  Error Message  -->
                    <#if messagesPerField.existsError('username','password')>
                        <div class="rounded-pill alert alert-danger mb-0 mt-2 p-0 d-flex justify-content-center text-danger">
                            ${kcSanitize(messagesPerField.getFirstError('username','password'))?no_esc}
                        </div>
                    </#if>

                    <!-- Remember me -->
                    <#if realm.rememberMe && !usernameEditDisabled??>
                        <div class="form-check mt-2 ms-3">
                            <#if login.rememberMe??>
                            <input class="form-check-input" type="checkbox" id="rememberMe" name="rememberMe" checked>
                            <#else>
                            <input class="form-check-input" type="checkbox" id="rememberMe" name="rememberMe">
                            </#if>
                            <label class="form-check-label text-success" for="rememberMe">
                                ${msg("rememberMe")}
                            </label>
                        </div>
                    </#if>

                    <#--  Sign In Button  -->
                    <input type="hidden" id="id-hidden-input" name="credentialId" <#if auth.selectedCredential?has_content>value="${auth.selectedCredential}"</#if>/>
                    <div class="d-flex flex-column align-items-center">
                        <button tabindex="4" type="submit" class="btn btn-primary text-success mt-3 rounded-pill py-1 px-5">${msg("doLogIn")}</button>

                        <!-- Forgot pw -->
                        <#if realm.resetPasswordAllowed>
                            <p class="m-0 mt-3 text-success"><a tabindex="5" style="text-decoration: none" href="${url.loginResetCredentialsUrl}">${msg("doForgotPassword")}</a></p>
                        </#if>
                    </div>     
                </form>
            </div>
            <!-- Register -->
            <#if realm.password && realm.registrationAllowed && !registrationDisabled??>
                <div class="d-flex flex-row justify-content-center align-items-center py-2 w-100 bg-primary" style="gap: 4px">
                    <p class="m-0 text-success">Don't have an account?</p>
                    <a href="${url.registrationUrl}" class="text-white" style="text-decoration: none">Sign Up</a>
                    <p class="m-0 text-success">here</p>
                </div>
            </#if>
        </div>

        <!-- Social providers -->
        <#if realm.password && social.providers??>
            <div class="card mt-3">
                <div class="card-body">
                    <h4 class="mb-3">${msg("identity-provider-login-label")}</h4>

                    <#list social.providers as p>
                        <a id="social-${p.alias}" class="btn btn-primary w-100 text-white"
                            type="button" href="${p.loginUrl}">
                            ${p.displayName!}
                        </a>
                    </#list>
                </div>
            </div>
        </#if>
    </#if>

</@layout.registrationLayout>
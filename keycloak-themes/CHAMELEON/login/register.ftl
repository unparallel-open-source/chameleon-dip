<#import "template.ftl" as layout>
<@layout.registrationLayout displayMessage=!messagesPerField.existsError('firstName','lastName','email','username','password','password-confirm'); section>
    <#if section = "header">
        ${msg("registerTitle")}
    <#elseif section = "form">
        <div class="card border-0 overflow-hidden" style="background-image: url(${url.resourcesPath}/img/loginBackground.png); background-position: 36% 63%; background-size: 154%; background-repeat: no-repeat; margin-top: 100px; background-color: #e7e8c5 !important; border-radius: 100px">
            <div style="max-width: 500px; padding-left: 80px; padding-right: 80px; padding-top: 41px; padding-bottom: 30px">

                <!-- Image -->
                <img src="${url.resourcesPath}/img/logoWithText.png" class="rounded-3" style="max-height: 94px; max-width: 100%"/> 

                <!-- Form -->
                <form id="kc-register-form" class="d-flex flex-column" action="${url.registrationAction}" method="post">
                    <div class="d-flex flex-row justify-content-between" style="margin-top: 37px; gap: 5px">

                        <!-- First Name -->
                        <div class="d-flex flex-column">
                            <input 
                                type="text" 
                                id="firstName" 
                                class="form-control rounded-pill" 
                                name="firstName" 
                                value="${(register.formData.firstName!'')}"
                                aria-invalid="<#if messagesPerField.existsError('firstName')>true</#if>"
                                placeholder="First Name"
                            />
                            <#if messagesPerField.existsError('firstName')>
                                <small
                                    id="input-error-firstname" 
                                    class="${properties.kcInputErrorMessageClass!} text-danger ms-2" 
                                    aria-live="polite">
                                        ${kcSanitize(messagesPerField.get('firstName'))?no_esc}
                                </small>
                            </#if>
                        </div>

                        <!-- Last Name -->
                        <div class="d-flex flex-column">
                            <input 
                                type="text" 
                                id="lastName" 
                                class="form-control rounded-pill" 
                                name="lastName"
                                value="${(register.formData.lastName!'')}"
                                aria-invalid="<#if messagesPerField.existsError('lastName')>true</#if>"
                                placeholder="Last Name"
                            />
                            <#if messagesPerField.existsError('lastName')>
                                <small 
                                    id="input-error-lastname" 
                                    class="${properties.kcInputErrorMessageClass!} text-danger ms-2" 
                                    aria-live="polite">
                                        ${kcSanitize(messagesPerField.get('lastName'))?no_esc}
                                </small>
                            </#if>
                        </div>
                    </div>    

                    <!-- Email -->
                    <div class="mt-3">
                        <input 
                            type="text" 
                            id="email" 
                            class="form-control rounded-pill" 
                            name="email"
                            value="${(register.formData.email!'')}" 
                            autocomplete="email"
                            aria-invalid="<#if messagesPerField.existsError('email')>true</#if>"
                            placeholder="Email"
                        />
                        <#if messagesPerField.existsError('email')>
                            <small id="input-error-email" class="${properties.kcInputErrorMessageClass!} text-danger ms-2" 
                            aria-live="polite">
                                ${kcSanitize(messagesPerField.get('email'))?no_esc}
                            </small>
                        </#if>
                    </div>

                    <#--  Username  -->
                    <#if !realm.registrationEmailAsUsername>
                        <div class="mt-3">
                            <input 
                                type="text" 
                                id="username" 
                                class="form-control rounded-pill" 
                                name="username"
                                value="${(register.formData.username!'')}" 
                                autocomplete="username"
                                aria-invalid="<#if messagesPerField.existsError('username')>true</#if>"
                            />

                            <#if messagesPerField.existsError('username')>
                                <small id="input-error-username" class="${properties.kcInputErrorMessageClass!} text-danger ms-2" aria-live="polite">
                                    ${kcSanitize(messagesPerField.get('username'))?no_esc}
                                </small>
                            </#if>
                        </div>
                    </#if>

                    <#--  Password  -->
                    <#if passwordRequired??>
                        <div class="mt-3">
                            <input 
                                type="password" 
                                id="password" 
                                class="form-control rounded-pill" 
                                name="password"
                                autocomplete="new-password"
                                aria-invalid="<#if messagesPerField.existsError('password','password-confirm')>true</#if>"
                                placeholder="Password"
                            />
                            <#if messagesPerField.existsError('password')>
                                <small id="input-error-password" class="${properties.kcInputErrorMessageClass!} text-danger ms-2" aria-live="polite">
                                    ${kcSanitize(messagesPerField.get('password'))?no_esc}
                                </small>
                            </#if>
                        </div>

                        <#--  Confirm Password  -->
                        <div class="mt-3">
                            <input 
                                type="password" 
                                id="password-confirm" 
                                class="form-control rounded-pill"
                                name="password-confirm"
                                aria-invalid="<#if messagesPerField.existsError('password-confirm')>true</#if>"
                                placeholder="Confirm Password"
                            />

                            <#if messagesPerField.existsError('password-confirm')>
                                <small id="input-error-password-confirm" class="${properties.kcInputErrorMessageClass!} text-danger ms-2" aria-live="polite">
                                    ${kcSanitize(messagesPerField.get('password-confirm'))?no_esc}
                                </small>
                            </#if>
                        </div>
                    </#if>

                    <#--  reCaptcha  -->
                    <#if recaptchaRequired??>
                        <div class="form-group">
                            <div class="${properties.kcInputWrapperClass!}">
                                <div class="g-recaptcha" data-size="compact" data-sitekey="${recaptchaSiteKey}"></div>
                            </div>
                        </div>
                    </#if>

                    <!-- Register Button -->
                    <div class="d-flex flex-column align-items-center">
                        <button class="btn btn-primary text-success mt-3 rounded-pill py-1 px-5" type="submit">${msg("doRegister")}</button>
                    </div>

                </form>
            </div>

            <!-- Back to Login -->
            <div class="d-flex flex-row justify-content-center align-items-center py-2 w-100 bg-primary" style="gap: 4px">
                <a href="${url.loginUrl}" class="text-white" style="text-decoration: none">Back to Login</a>
            </div>

        </div>
    </#if>
</@layout.registrationLayout>


/** @type {import('next').NextConfig} */
const nextConfig = {
    eslint: {
        // Warning: This allows production builds to successfully complete even if
        // your project has ESLint errors.
        ignoreDuringBuilds: true,
    },
    rewrites: async () => {
        return [
            {
                source: '/iot-catalogue/api/data/:path*',
                destination: `${process.env.IOT_CAT_URL}/api/data/:path*`
            },
        ]
    }
}

module.exports = nextConfig
# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]
- Edited error message component on login page
- Updated login page
- Updated register page
- Updated Keycloak default configuration: Session timeouts, Email as username
- Fixed Keycloak error on user register

## [0.16.4] - 2024-09-11
- Added theme configuration to dev deployment

## [0.16.3] - 2024-09-11
- Added sharp to optimize image generation

## [0.16.2] - 2024-09-11
- Fixed footer margin on mobile

## [0.16.1] - 2024-09-10
- Fixed footer margins and overflow behaviour

## [0.16.0] - 2024-09-05
- Fixed size of the cart when logged in
- Changed footer to fixed position and infinite
- Added logo buttons to social media
- New footer EU disclamer

## [0.15.0] - 2024-07-16
- Added execution pages

## [0.14.2] - 2024-07-12
- Fixed bug on IoTCatIframe component where the session information is not properly handled

## [0.14.1] - 2024-07-09
- Updated register card on mobile
- Added padding to the header on mobile
- Updated bundles banner responsivity
- Updated responsiveness of homepage counters

## [0.14.0] - 2024-06-26
- Fixed size for image in Welcome page
- Fixed responsivity of dataset explorer anf bundle page legends
- Updated breakpoints - removed XXL (1400px)
- Updated responsiveness of homepage
- Added Bundles banner

## [0.13.2] - 2024-06-26
- Fix bug with missing "remove" button for bundle and dataset list

## [0.13.1] - 2024-06-21
- Fixed bug with filter

## [0.13.0] - 2024-06-21
- Added counts for: datasets, forestry, agriculture and livestock
- Fixed dataset legend margins

## [0.12.1] - 2024-06-21
- obtaining token on Iframe component

## [0.12.0] - 2024-06-21
- Changed authorization support

## [0.11.0] - 2024-06-21
- Added variable IOT_CAT_PUBLIC_TOKEN and  IOT_CAT_PUBLIC_CONTEXT to deployment file
- Content accessible without login
- Included navbar "user" dropdown menu version for no login
- Added support for private and public counts and integration
- Content accessible without login
- Included navbar "user" dropdown menu version for no login
- Homepage with no login version
- Fixed homepage colors and spacing
- New homepage counter icons
- Updated data models page


## [0.10.0] - 2024-06-20
- Added component card color costumization on bundle page

## [0.9.1] - 2024-06-19
- Removed ".tsx" from import statement

## [0.9.0] - 2024-06-19
- Added new icons and counting
- Add iframe communication (postMessage) to change the filter of IoT Catalogue list
- Added swagger page
- Updated deployment yaml
- Fixed margins
- Updated to transparent background iframes
- Included background images
- Updated page headers
- Added "Show all tools" button in bundles page
- Page components only render after loading list
- Fixed bundle background position

## [0.8.6] - 2024-05-23
- Added updateBundleOptions
- First release
- Login page
- Welcome page with simple cards showing authentication data
- Fixed bug with session
- Added IoT Catalogue integration
- Added data model page
- Added My Executions page
- Added bundle filter to bundle page
- Added right og image

## [0.1.0] - 2023-09-22

- CI support

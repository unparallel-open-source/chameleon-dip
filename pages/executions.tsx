import Image from "react-bootstrap/Image";
import ValidationList from "../components/ValidationList/ValidationList";
import { executionTagId } from "../lib/constants";
const filter = { tags: executionTagId }
export default function Executions() {
    function renderValidationList() {
        return <ValidationList filter={filter} />
    }
    return (
        <div className={"container"}>
            <div style={{ position: "relative" }}>
                <Image src="/bundleBackground.png" width={400} style={{ position: "absolute", zIndex: "-1", marginLeft: "-170px", marginTop: "-5px" }} />
            </div>
            <h1 className="text-dark-green" style={{ marginTop: "80px" }}>Executions</h1>
            {renderValidationList()}
        </div>
    )
}

import { useEffect, useState, useRef, useMemo } from "react";
import ComponentList from "../components/ComponentList/ComponentList";
import { dataSetTagId, dataModelTagId, bundleTagId } from "../lib/constants"

import { addBundlesToOrder, getCurrentOrder, removeBundleFromOrder } from "../lib/helper/mongodb/index"
import Loading from "../components/Loading/Loading"
import { useRouter } from 'next/navigation'
import { useData } from "/lib/dataFetch";
import FilterIcon from "/components/icons/filter/index.tsx"
import Image from "react-bootstrap/Image";
import { useIframeResize } from "../components/useIframeResize";
import classnames from 'classnames';
import styles from "./bundles.module.scss"
import { useSession } from "next-auth/react";
import { renderNumber } from "../lib/methods"
const bundleFilter = {
	$and: [
		{ tags: { $nin: [dataSetTagId, dataModelTagId] } },
		{ tags: bundleTagId }
	]
}


const bundleWithToolsFilter = { tags: { $nin: [dataSetTagId, dataModelTagId] } }

export default function BundlesPage() {




	const router = useRouter()
	const [order, setOrder] = useState()
	const bundleCount = useData("/api/count/bundles")?.data
	const toolCount = useData("/api/count/tools")?.data

	const [showAll, setShowAll] = useState(false)
	const ref = useRef(undefined)
	const { height } = useIframeResize(ref)

	const [filter, setFilter] = useState(bundleFilter)

	useEffect(() => {
		if (showAll) setFilter(bundleWithToolsFilter)
		else setFilter(bundleFilter)
	}, [showAll])

	useEffect(() => {
		getCurrentOrder().then(setOrder)
	}, [])

	function onComponentAdded(id) {
		console.log("onComponentAdded")
		if (order.bundles && order.bundles.length > 0) {
			alert("Only one bundle can be selected")
			return
		}
		addBundlesToOrder([id]).then(() => {
			router.push("/")
		})
	}
	function onComponentRemoved(id) {
		removeBundleFromOrder(id).then(() => {
			router.push("/")
		})
	}
	if (!order) return <Loading />


	function renderIcon(label, color = "var(--bs-primary)", disabled = false) {
		let style
		if (disabled) style = { filter: "grayscale(1)", opacity: 0.5 }
		return (
			<div className="d-flex" style={style}>
				<FilterIcon className={classnames(height < 400 ? "d-none" : "me-2 align-items-center")} width={20} color={color} />
				<p className={classnames(height < 400 ? "d-none" : "me-4 mb-0")}>{label}</p>
			</div>
		)
	}

	function renderCounts() {
		return (
			<div className="d-flex">
				{renderIcon(`Showing ${renderNumber(bundleCount)} Bundles`)}
				{renderIcon(`Showing ${renderNumber(toolCount)} Tools`, "var(--bs-info)", !showAll)}
			</div>
		)
		return (
			<div className="d-flex">
				<p className="me-4">Showing {bundleCount} Bundles</p>
				<p>Showing {toolCount} Tools</p>
			</div>
		)
	}
	function renderShowAll() {
		if (!showAll) {
			return (
				<>
					<button onClick={() => setShowAll(true)} className={classnames(height < 400 ? "d-none" : "d-none d-sm-block btn btn-primary btn-sm text-white rounded-pill align-self-end")} style={{ position: "relative", marginTop: "-58px", minWidth: "164px" }}>Show all available tools</button>
					<button onClick={() => setShowAll(true)} className={classnames(height < 400 ? "d-none" : "d-block d-sm-none btn btn-primary btn-sm text-white rounded-pill align-self-center mb-4")} style={{ position: "relative", marginTop: "0px", minWidth: "164px" }}>Show all available tools</button>
				</>

			)
		}
		else {
			return (
				<>
					<button onClick={() => setShowAll(false)} className={classnames(height < 400 ? "d-none" : "d-none d-sm-block btn btn-info btn-sm text-white rounded-pill align-self-end")} style={{ position: "relative", marginTop: "-58px", minWidth: "164px" }}>Hide tools</button>
					<button onClick={() => setShowAll(false)} className={classnames(height < 400 ? "d-none" : "d-block d-sm-none btn btn-info btn-sm text-white rounded-pill align-self-center mb-4")} style={{ position: "relative", marginTop: "0px", minWidth: "164px" }}>Hide tools</button>
				</>
			)
		}
	}

	function renderComponentList() {
		return (
			<>
				<div className="">
					<ComponentList componentListProps={componentListProps} className={styles.components} addedComponentIds={order.bundles} filter={filter} onComponentAdded={onComponentAdded} onComponentRemoved={onComponentRemoved} />
				</div>

			</>
		)
	}

	const style = getComputedStyle(document.body)
	const color = style.getPropertyValue('--bs-info')

	const elementColorsByFilters = [
		{
			color,
			filter: { tags: { $ne: bundleTagId } }
		}
	]
	const componentListProps = { elementColorsByFilters }
	return (
		<div className={"container"}>
			<div style={{ position: "relative" }}>
				<Image src="/bundleBackground.png" width={400} style={{ position: "absolute", zIndex: "-1", marginLeft: "-170px", marginTop: "-5px" }} />
			</div>
			<h1 className="text-dark-green" style={{ marginTop: "80px" }}>Bundles</h1>
			{renderCounts()}
			<div className="d-flex flex-column">
				{renderComponentList()}
				{/*<div className="d-flex justify-content-end" style={{position: "relative", paddingRight: "0px", margssinTop: "-64px"}}>*/}
				{renderShowAll()}
				{/*</div>*/}
			</div>
		</div>
	);
}

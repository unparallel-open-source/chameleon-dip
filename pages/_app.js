import "../styles/theme.scss";
import { SessionProvider } from "next-auth/react";
import Layout from "../components/layout/layout";
import { useCart } from "cart";

export default function App({
    Component,
    pageProps: {session, ...pageProps},
}){


    return (
        <SessionProvider session={session}>
            <Layout home>
                <Component {...pageProps} />
            </Layout>
        </SessionProvider>
    );
}

import IoTCatIframe from "../components/IoTCatIframe";
import React, { useEffect, useState } from "react";
import { getIoTCatComponents, getOrders, } from "../lib/helper/mongodb"
import { Order } from "../components/types";
import _ from "lodash";
import dayjs from 'dayjs'
import Loading from "../components/Loading/Loading";

export default function Executions() {

	const [orders, setOrders] = useState<Order[] | null>(null)

	const [componentsHM, setComponentsHM] = useState<{ [k: string]: any | null }>(null)

	useEffect(() => {
		getOrders({ status: "EXECUTING" }, undefined, { sort: { createdOn: 1 } }).then(setOrders)
	}, [])


	async function getComponents() {
		if (!orders) return
		const images = orders.filter(e => e.images).reduce((a, v) => ([...a, ...v.images]), [])
		const bundles = orders.filter(e => e.bundles).reduce((a, v) => ([...a, ...v.bundles]), [])
		const componentIds = _.uniq([...images, ...bundles])
		const components = await getIoTCatComponents(componentIds)
		const _componentsHM: { [k: string]: any } = {}
		for (const component of components) {
			_componentsHM[component._id] = component
		}
		setComponentsHM(_componentsHM)

	}


	useEffect(() => {
		getComponents()
	}, [orders])

	if (!componentsHM) return <Loading />

	function formatDate(date: string | Date) {
		return dayjs(date).format("HH:mm:ss DD-MM-YYYY")
	}


	function renderRow(order: Order) {
		return (
			<tr key={order._id}>

				<td>{formatDate(order.createdOn)}</td>
				<td>{renderComponentNames(order.images)}</td>
				<td>{renderComponentNames(order.bundles)}</td>
			</tr>
		)
	}


	function renderComponentNames(ids: string[]) {
		if (!ids) return ""
		return ids.map(id => componentsHM[id]).map(el => el.name).join(", ")
	}

	function renderTable() {
		if (orders.length === 0) return <h2 className={"text-dark-green"}>No orders being executed</h2>
		return (
			<table className="table ">
				<thead>
					<tr className="row-primary">
						<th scope="col">Creation Date</th>
						<th scope="col">Images</th>
						<th scope="col">Bundle</th>
					</tr>
				</thead>
				<tbody>
					{orders.map(renderRow)}

				</tbody>
			</table>
		)
	}

	return (
		<div className="container">
			<h1 className={"text-dark-green"}>Execution List</h1>
			{renderTable()}
		</div>
	)
	return (
		<div className={"container"}>
			<h1 className={"text-dark-green"}>Execution List</h1>
			<IoTCatIframe pageName={"validations"} />

		</div>

	)
}
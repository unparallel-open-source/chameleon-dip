import { useRouter } from "next/router";
import { useRef, useEffect, useState } from 'react'
import { IoTCatURL } from "../../components/misc.js"
import IoTCatIframe from "../../components/IoTCatIframe";

import { addImagesToOrder, getCurrentOrder, addBundlesToOrder, removeBundleFromOrder } from "../../lib/helper/mongodb/index"
import Loading from "../../components/Loading/Loading"
import { Order } from "../../components/types.js";


export default function IoTCatComponent() {

	const [order, setOrder] = useState<Order | null>(null)
	useEffect(() => {
		getCurrentOrder().then(setOrder)
	}, [])


	const router = useRouter();
	const { id } = router.query
	if (!order) return <Loading />

	function onComponentAdded(id: string) {
		addBundlesToOrder([id]).then(() => {
			router.push("/")
		})

	}
	function onComponentRemoved(id: string) {
		removeBundleFromOrder(id).then(() => {
			router.push("/")
		})
	}
	const componentIds = [...(order.bundles || []), ...(order.images || [])]

	return (
		<div className={"container"}>
			<IoTCatIframe pageName={`components/${id}`} addedComponentIds={componentIds} onComponentAdded={onComponentAdded} onComponentRemoved={onComponentRemoved} />
		</div>
	)

}
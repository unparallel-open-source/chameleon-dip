import { NextApiRequest, NextApiResponse } from "next";
import { connectToDatabase } from "../../../lib/helper/connectMongoDB";
import { getComponent } from "../component/[id]";

export default async function getBundles(req: NextApiRequest, res: NextApiResponse) {
    try {
        let { db } = await connectToDatabase();
        const pendingOrder = await db.collection("orders").findOne({ status: "PENDING" })
        const bundleIds = pendingOrder?.bundles || []
        //const bundles = await db.collection("bundles").find(req.body.filters || {}).project(req.body.projection || {}).toArray();
        const bundles = await Promise.all(bundleIds.map(getComponent))

        res.status(200).json({ bundles });
    } catch (error) {
        console.error(error);
        res.status(500).json({ error });
    }
}

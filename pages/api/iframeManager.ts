import { NextApiRequest, NextApiResponse } from "next";
import { generateURLPath, URLRequest } from "./urlManager";
import { bundleTagId } from "../../lib/constants";
import { getServerSession } from "next-auth";
///api/auth/[...nextauth]/route'


import { cookies } from 'next/headers'
import { getIoTCatParams } from "../../lib/methods";
export default async function iframeManager(req: NextApiRequest, res: NextApiResponse) {
    try {


        const { pageName, filter, showAddButton, addedComponentIds, componentListProps, origin } = req.body || {}

        if (!pageName) throw { status: 500 }
        let customProps: any
        if (showAddButton) {
            customProps = {
                "componentCard": {
                    "enableAddButton": true,
                    addedComponentIds
                }
            }
        }



        const { context, token } = getIoTCatParams(req)

        const requests: URLRequest[] = [
            {
                "id": "index",
                "json": {
                    context,
                    token,
                    "useResizeSensor": true,
                    customProps,
                    origin

                }
            }
        ]

        if (filter) {
            requests.push({
                "id": "componentList",
                "json": {
                    filter, ...componentListProps, showOnlyBody: true
                }
            })
            requests.push({
                "id": "validationList",
                "json": {
                    filter, showOnlyBody: true
                }
            })


        }

        const value = await generateURLPath(requests)
        const url = `${process.env.IOT_CAT_URL}/embedded/${pageName}?${value}`
        res.status(200).json(url);
    } catch (error) {
        console.error(error);
        res.status(error.status || 500).json({ error });
    }
}

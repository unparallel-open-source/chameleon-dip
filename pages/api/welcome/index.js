import {catchErrors} from "../../../lib/dataFetch";

export default catchErrors(async (req, res) => {
    const result = await fetch(
        "https://www.randomnumberapi.com/api/v1.0/random?count=4"
    );
    const data = await result.json();

    return data;
});

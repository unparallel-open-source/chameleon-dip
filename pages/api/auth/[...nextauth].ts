import NextAuth, { NextAuthOptions } from "next-auth";
import KeycloakProvider from "next-auth/providers/keycloak";


export default NextAuth({
    // Configure one or more authentication providers
    providers: [
        // ...add more providers here
        KeycloakProvider({
            clientId: process.env.AUTH_CLIENT_ID,
            clientSecret: process.env.AUTH_CLIENT_SECRET,
            issuer: `${process.env.AUTH_ISSUER_LOCAL_URL}/realms/${process.env.AUTH_ISSUER_REALM}`,
            authorization: {
                params: {
                  scope: "openid email profile",
                },
                url: `${process.env.AUTH_ISSUER_LOCAL_URL}/realms/${process.env.AUTH_ISSUER_REALM}/protocol/openid-connect/auth`,
              },
            token: `${process.env.AUTH_ISSUER_CONTAINER_ENDPOINT}/realms/${process.env.AUTH_ISSUER_REALM}/protocol/openid-connect/token`,
            userinfo: `${process.env.AUTH_ISSUER_CONTAINER_ENDPOINT}/realms/${process.env.AUTH_ISSUER_REALM}/protocol/openid-connect/userinfo`,
            jwks_endpoint: `${process.env.AUTH_ISSUER_CONTAINER_ENDPOINT}/realms/${process.env.AUTH_ISSUER_REALM}/protocol/openid-connect/certs`,
            wellKnown: undefined
        })
    ],
    callbacks: {
        async jwt({ token, account }) {
            // Persist the OAuth access_token to the token right after signin
            if (account)
                token.accessToken = account.access_token;

            return token;
        },
        async session({ session, token, user }) {
            // Send properties to the client, like an access_token from a provider.
            //@ts-ignore
            session.accessToken = token.accessToken;

            return session;
        }
    }
})

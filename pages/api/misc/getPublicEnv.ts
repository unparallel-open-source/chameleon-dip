//	const key = context.params.key
// 	return NextResponse.json(process.env[`NEXT_PUBLIC_${key}`] || "")

import { NextApiRequest, NextApiResponse } from "next";


export default async function getPublicEnv(req: NextApiRequest, res: NextApiResponse) {
	try {
		//return NextResponse.json(process.env[`NEXT_PUBLIC_${key}`] || "")
		const publicEnvs = {}
		const keys = Object.keys(process.env).filter(el => el.startsWith("NEXT_PUBLIC_"))
		for (const key of keys) {
			publicEnvs[key] = process.env[key]
		}
		res.status(200).json(publicEnvs);
	} catch (error) {
		console.error(error);
		res.status(500).json({ error });
	}
}

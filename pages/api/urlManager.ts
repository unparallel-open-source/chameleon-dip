import { NextApiRequest, NextApiResponse } from "next";


export interface URLRequest {
    id: string,
    json: any
}

export async function generateURLPath(requests: URLRequest[]) {
    const url = `${process.env.IOT_CAT_URL}/api/generateURLParameters`
    const body = JSON.stringify({ requests })
    const response = await fetch(url, {
        method: "POST",
        headers: {
            "Content-Type": "application/json",
            "Authorization": `Bearer ${process.env.IOT_CAT_TOKEN}`
        },
        body
    })
    if (response.status !== 200) {
        throw { status: response.status, err: "Invalid request possible not logged in" }
    }

    return response.json()
}



export default async function urlManager(req: NextApiRequest, res: NextApiResponse) {
    try {
        const value = await generateURLPath([{ id: "xxx", json: { m: 123 } }])
        //  const json = req.body



        res.status(200).json({ value });
    } catch (error) {
        console.error(error);
        res.status(error.status || 500).json({ error });
    }
}

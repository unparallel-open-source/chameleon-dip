import { ObjectId } from "mongodb";
import { NextApiRequest, NextApiResponse } from "next";
import { connectToDatabase } from "../../../lib/helper/connectMongoDB";

interface Order {
    _id: string,
    name: string
}

export default async function addOrder(req: NextApiRequest, res: NextApiResponse){
    try{
        let { db } = await connectToDatabase();

        if(!req.body.order)  throw {msg:"Empty body"};

        const order = {
            _id: (new ObjectId()).toString(),
            ...req.body.order
        };
        const insertedOrder = await db.collection<Order>("orders").insertOne(order);

        res.status(200).json({ insertedOrder });
    } catch(error){
        console.error(error);
        res.status(500).json({ error });
    }
}

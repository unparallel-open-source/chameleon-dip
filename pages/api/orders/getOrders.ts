import { NextApiRequest, NextApiResponse } from "next";
import { connectToDatabase } from "../../../lib/helper/connectMongoDB";

export default async function getOrders(req: NextApiRequest, res: NextApiResponse) {
    try {
        let { db } = await connectToDatabase();
        const orders = await db.collection("orders").find(req.body.filters || {}, req.body.options || {}).project(req.body.projection).toArray();

        res.status(200).json({ orders });
    } catch (error) {
        console.error(error);
        res.status(500).json({ error });
    }
}

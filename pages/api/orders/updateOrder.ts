import { NextApiRequest, NextApiResponse } from "next";
import { connectToDatabase } from "../../../lib/helper/connectMongoDB";

export default async function updateOrder(req: NextApiRequest, res: NextApiResponse) {
    try {
        let { db } = await connectToDatabase();

        if (!req.body.filters || !req.body.options) throw { msg: "No filter or options were provided" }

        const updatedOrder = await db.collection("orders").updateOne(req.body.filters, req.body.options);

        res.status(200).json({ updatedOrder });
    } catch (error) {
        console.error(error);
        res.status(500).json({ error });
    }
}

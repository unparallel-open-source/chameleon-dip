import { NextApiRequest, NextApiResponse } from "next";
import { connectToDatabase } from "../../../lib/helper/connectMongoDB";
import { getComponent } from "../component/[id]";

export default async function getImages(req: NextApiRequest, res: NextApiResponse) {
    try {
        let { db } = await connectToDatabase();
        const pendingOrder = await db.collection("orders").findOne({ status: "PENDING" })
        const imageIds = pendingOrder?.images || []

        const images = await Promise.all(imageIds.map(getComponent))

        res.status(200).json({ images });
    } catch (error) {
        console.error(error);
        res.status(500).json({ error });
    }
}

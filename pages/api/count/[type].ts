//http://127.0.0.1:4000/api/getTPIElement?pageName=components&id=xMAaRY2DHfbngu8mD

import { NextApiRequest, NextApiResponse } from "next";
import { getIoTCatParams } from "../../../lib/methods";

export async function getComponent(id: string) {

    const url = `${process.env.IOT_CAT_URL}/api/data/components/${id}`
    const response = await fetch(url,
        {
            headers: {
                "Authorization": `Bearer ${process.env.IOT_CAT_TOKEN}`
            }
        })
    return response.json()

}

const dataModelTagId = "9ce365595b7b7341fcb59c10"
const bundleTagId = "e8b6b0cb04218ae939265061"

const forestryTagId = "acfe53109d6987a99a116605"
const agricultureTagId = "5b45f17f7e5cd662c57acd62"
const livestockTagId = "5e68fbba353ec6d3c4805e04"
const dataSetTagId = "a265ebe9a8a87e5399be9527"
const countPaths: { [k: string]: string } = {
    bundles: "/api/count/bundles",
    tools: `/api/count/components?excludeTags=${dataModelTagId},${bundleTagId}`,
    forestry: `/api/count/components?includeTags=${forestryTagId}`,
    agriculture: `/api/count/components?includeTags=${agricultureTagId}`,
    livestock: `/api/count/components?includeTags=${livestockTagId}`,
    datasets: `/api/count/dataSets`
}

export default async function component(req: NextApiRequest, res: NextApiResponse) {
    try {
        // const value = await generateURLPath([{ id: "xxx", json: { m: 123 } }])
        //  const json = req.body
        const { token } = getIoTCatParams(req)
        const type = req.query.type
        if (!type) throw { status: 404, msg: "Element not found" }
        const path = countPaths[type as string]
        if (!path) throw { status: 404, msg: "Element not found" }
        const url = `${process.env.IOT_CAT_URL}${path}`
        const response = await fetch(url,
            {
                headers: {
                    "Authorization": `Bearer ${token}`
                }
            })
        const json = await response.json()

        res.status(200).json(json);
    } catch (error) {
        console.error(error);
        res.status(error.status || 500).json({ error });
    }
}

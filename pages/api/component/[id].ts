//http://127.0.0.1:4000/api/getTPIElement?pageName=components&id=xMAaRY2DHfbngu8mD

import { NextApiRequest, NextApiResponse } from "next";

export async function getComponent(id: string) {

    const url = `${process.env.IOT_CAT_URL}/api/data/components/${id}`
    const response = await fetch(url,
        {
            headers: {
                "Authorization": `Bearer ${process.env.IOT_CAT_TOKEN}`
            }
        })
    return response.json()

}

export default async function component(req: NextApiRequest, res: NextApiResponse) {
    try {
        // const value = await generateURLPath([{ id: "xxx", json: { m: 123 } }])
        //  const json = req.body

        const id = req.query.id
        if (!id) throw { status: 404, msg: "Element not found" }

        const json = await getComponent(id as string)
        res.status(200).json(json);
    } catch (error) {
        console.error(error);
        res.status(error.status || 500).json({ error });
    }
}

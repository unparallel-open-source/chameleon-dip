import ComponentList from "../components/ComponentList/ComponentList";
import { dataModelTagId } from "../lib/constants"
import Image from "react-bootstrap/Image";

const filter = { tags: dataModelTagId }

export default function DatamodelsPage() {
    return (
        <div className={"container"}>
            <div style={{ position: "relative" }}>
				<Image src="/datamodelBackground.png" width={400} style={{ position: "absolute", zIndex: "-1", marginLeft: "-170px", marginTop: "-5px" }} />
			</div>
            <h1 className="text-dark-green" style={{ marginTop: "80px" }}>Data Models</h1>
            <ComponentList filter={filter} />
        </div>
    );
}

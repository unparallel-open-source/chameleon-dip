
import IoTCatIframe from "../components/IoTCatIframe";
import Loading from "../components/Loading/Loading";
import { useEffect, useState } from "react";

export default function IoTCatSwagger() {
    const [swaggerId, setSwaggerId] = useState<string | undefined>()

    async function getId() {
        const response = await fetch("/api/misc/getPublicEnv")
        const json = await response.json()
        const id = json.NEXT_PUBLIC_SWAGGER_ID
        return id
    }


    useEffect(() => {
        getId().then(setSwaggerId)

    }, [])

    if (!swaggerId) return <Loading />
    return (
        <div className={"container"}>
            <IoTCatIframe pageName={`swagger/${swaggerId}`} />
        </div>
    )
}
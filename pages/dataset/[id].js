import { useRouter } from "next/router";
import { useRef, useEffect, useState } from 'react'
import { IoTCatURL } from "../../components/misc.js"
import IoTCatIframe from "../../components/IoTCatIframe";
import { addImagesToOrder, getCurrentOrder, addBundlesToOrder, removeImageFromOrder } from "../../lib/helper/mongodb/index"
import Loading from "../../components/Loading/Loading"
export default function Dataset() {

	const [order, setOrder] = useState(null)
	useEffect(() => {
		getCurrentOrder().then(setOrder)
	}, [])


	const router = useRouter();
	const { id } = router.query
	if (!order) return <Loading />


	function onComponentAdded(id) {
		addImagesToOrder([id]).then(() => {
			router.push("/")
		})

	}
	function onComponentRemoved(id) {
		removeImageFromOrder(id).then(() => {
			router.push("/")
		})
	}
	const componentIds = [...(order.bundles || []), ...(order.images || [])]
	return (
		<div className={"container"}>
			<IoTCatIframe pageName={`dataSets/${id}`} addedComponentIds={componentIds} onComponentAdded={onComponentAdded} onComponentRemoved={onComponentRemoved} />
		</div>
	)

}
import {useRouter} from "next/router";
import IoTCatIframe from "../../components/IoTCatIframe";

export default function IoTCatValidation() {
	const router = useRouter();
	const { id } = router.query
	return (
		<div className={"container"}>
			<IoTCatIframe pageName={`validations/${id}`}  />
		</div>
	)

}
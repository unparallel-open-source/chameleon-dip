import Layout, { siteTitle } from "../components/layout/layout";
import Head from "next/head";
import { useSession } from "next-auth/react";

import Login from "../components/login/login";
import Welcome from "../components/welcome/welcome";
import Loading from "../components/Loading/Loading"


export default function Home() {
    const { data: session } = useSession();


    function renderContent() {
        if (session === null) return <Login />
        if (session) return <Welcome />
        return <Loading />
    }

    return (
        <>
            <Head>
                <title>{siteTitle}</title>
                <meta name={"description"} content={"Generated by create next app"} />
                <link rel={"icon"} href={"/favicon.png"} />
            </Head>
            <Welcome/>
            {/*{renderContent()}*/}

        </>

    );
}

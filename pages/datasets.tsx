import DatasetList from "../components/DatasetList/DatasetList";

export default function DatasetPage() {
	return (
		<div className={"container"}>

			<DatasetList />
		</div>

	);
}

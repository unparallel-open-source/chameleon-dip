# CHAMELEON Drone Innovation Platform (DIP)

## **Requirements**

- Node (tested on v18.12.0)
- Docker (tested on v4.25.2)
- Visual Studio Code (only mandatory for dev containers)
<br></br>

## **Installation**

First step is to clone the project from GitLab

```shell
git clone https://gitlab.com/unparallel-open-source/chameleon-dip.git
```

The application can be launched in 3 different modes
- All in Docker containers
- Main code running locally | support components in Docker containers
- Dev container

Running **everithing in docker** allows you to abstract from an IDE, making it the simplest approach. Running the **main code locally** allows you to see the changes instantly, making it better for developing. **Dev containers** offer the same features as running locally, with the added bonus of not having to deal with any dependencies but sacrificing some performance.
<br></br>

### **All docker containers**

To run all the docker containers, use the folowing commands
```shell
docker compose build
docker compose up -d
```
&nbsp;

### **Main code locally**

First, launch the support components
```shell
docker compose up -d mongodb keycloak
```

Then you can use the "Run and Debug" command from VSCode:

<div align="center"><img src="public/VSCode run local.png" width="50%"></div>

or run the following command
```shell
npm run dev
```
&nbsp;

### **Dev container**

To launch a dev container, VSCode is required. First, launch the support components
```shell
docker compose up -d mongodb keycloak
```

Then reqbuild and reopen the project inside a dev container, using the VSCode comand

<div align="center"><img src="public/Dev container comand.png" width="50%"></div>

Once open inside the dev container, use "Run and Debug" command specific for dev containers

<div align="center"><img src="public/VSCode dev container.png" width="50%"></div>

or run the following command

```shell
npm run dev
```
&nbsp;
### **Access the project**

For any of the above scenarios, a successfuly launched application will be available on http://localhost:3000.
<br></br>

### **Useful links**

- Install Node and NPM: https://docs.npmjs.com/downloading-and-installing-node-js-and-npm <br>
- Docker: https://www.docker.com <br>
- Visual Studio Code: https://code.visualstudio.com <br>

## **Contributing**

In order to contribute to this project, the following technical details are of high relevance.

### **Helper**

By importing the <code>lib/helper</code> directory into a React component, you will have access to these methods:

| Category | Method | Description |
|----------|--------|-------------|
| Keycloak | <code>getKeycloakSession</code> | Returns the Keycloak session object. |
| Keycloak | <code>getKeycloakToken</code> | Returns the Keycloak session token. |
| Keycloak | <code>getKeycloakUserID</code> | Returns the Keycloak ID of the logged in user. |
| MongoDB | <code>getBundles</code> | Returns the bundles present in the database, according to the provided filters. |
| MongoDB | <code>getBundleDefaultOptions</code> | Returns the default options of a bundle, given its ID. |
| MongoDB | <code>getImages</code> | Returns the images present in the database, according to the provided filters. |
| MongoDB | <code>addOrder</code> | Adds the given order to the database. |
| MongoDB | <code>getOrders</code> | Returns the orders present in the database, according to the provided filters. |
| MongoDB | <code>getCurrentOrder</code> | Returns the order whose status is <code>PENDING</code>. |
| MongoDB | <code>updateOrder</code> | Updates the order with the provided information (e.g., bundle, images). |
| MongoDB | <code>addBundlesToOrder</code> | Adds the bundle(s) with the provided ID(s) to the order. |
| MongoDB | <code>updateBundleOptions</code> | Updates the bundle options selected for the current order, given the bundle ID. |
| MongoDB | <code>removeBundleFromOrder</code> | Removes a bundle from the current order, given its ID. |
| MongoDB | <code>addImagesToOrder</code> | Adds the image(s) with the provided ID(s) to the order. |
| MongoDB | <code>removeImageFromOrder</code> | Removes an image from the current order, given its ID. |

Further details are available in the [Wiki](https://gitlab.com/unparallel-open-source/chameleon-dip/-/wikis/CHAMELEON-Drone-Innovation-Platform-(DIP)).

## **Acknowledgements**

The Drone Innovation Platform (DIP) was developed under the scope
of the [CHAMELEON](https://chameleon-heu.eu/) research project, which has received funding from the European Union's
Horizon Europe research and innovation programme under grant agreement no. 101060529.

## **License**
[MIT License](https://choosealicense.com/licenses/mit/)
